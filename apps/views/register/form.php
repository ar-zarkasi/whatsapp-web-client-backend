<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\bootstrap5\ActiveForm;
use yii\bootstrap5\Html;

$this->title = 'Registrasi Akun';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card">
    <div class="card-body">
        <div class="m-sm-4">
            <?php $form = ActiveForm::begin([
                'id'=>'registrasi-form',
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"mb-3\">{input}{error}</div>\n",
                    'labelOptions' => ['class' => 'form-label'],
                ],
                'options'=>[
                    'autocomplete'=>'off',
                ]
            ]); ?>
                <div class="mb-3">
                    <?= $form->field($model, 'name')->textInput([
                        'placeholder'=>'Nama Lengkap',
                        'class'=>'form-control form-control-lg',
                    ]) ?>
                </div>
                <div class="mb-3">
                    <?= $form->field($model, 'email')->textInput([
                        'placeholder'=>'Alamat Email',
                        'class'=>'form-control form-control-lg',
                    ]) ?>
                </div>
                <div class="mb-3">
                    <?= $form->field($model, 'phone')->textInput([
                        'placeholder'=>'Nomor Telepon Aktif',
                        'class'=>'form-control form-control-lg',
                    ]) ?>
                </div>
                <div class="mb-3">
                    <?= $form->field($model, 'password')->passwordInput([
                        'placeholder'=>'Password yang akan anda gunakan',
                        'class'=>'form-control form-control-lg',
                        'label'=>false,
                    ]) ?>
                </div>
                <div class="mb-3">
                    <?= $form->field($model, 'cpassword')->passwordInput([
                        'placeholder'=>'Konfirmasi Ulang Password',
                        'class'=>'form-control form-control-lg',
                        'label'=>false,
                    ]) ?>
                </div>
                <div class="mb-3">
                    <div class="card">
                        <div class="card-body bg-warning p-4">
                            <i>Dengan Melakukan Pendaftaran ini saya setuju untuk mematuhi segala aturan dari pihak Technergy terkait aplikasi ini</i>
                        </div>
                    </div>
                </div>
                <div>
                    <?= $form->field($model, 'agree')->checkbox([
                        'template' => "<label class=\"form-check\">{input}\n{label}\n{error}</label>",
                    ]) ?>
                </div>
                <div class="text-center mt-3">
                    <?= Html::submitButton('Sign Up', ['class' => 'btn btn-lg btn-primary', 'name' => 'register-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>