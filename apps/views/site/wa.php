<?php
use yii\bootstrap5\Html;
?>
<div class="row mb-3 mt-3">
    <div class="col-12 col-md-6">
        <div class="card">
            <img class="card-img-top" id="image-qr" default-src="img/photos/unsplash-1.jpg" src="img/photos/unsplash-1.jpg" alt="Unsplash">
            <div class="card-header">
                <h5 class="card-title mb-0">Whatsapp Web Status</h5>
            </div>
            <div class="card-body">
                <p class="card-text" id="message-log">Whatsapp Not Ready</p>
                <button type="button" class="btn btn-primary" id="start-wa">Start Whatsapp Web</a>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div id="image-qr"></div>
    </div>
    <div class="col-12 mt-2">
        <div id="message-log" class="fs-6">
        </div>
    </div>
</div>
<?php 
$this->registerJs(<<<JS
    const idlogin = "$login";
    const socket = io('http://localhost:8000/' + idlogin);
    const msg = document.getElementById('message-log');
    const imagediv = document.getElementById('image-qr');
    const btnstart = document.getElementById('start-wa')
    
    btnstart.setAttribute('disabled','disabled');

    socket.on('disconnect',() => {
        msg.innerHTML = "Whatsapp Server Not Ready";
        btnstart.setAttribute('disabled','disabled');
    });
    socket.on('qr', (data) => {
        imagediv.setAttribute('src', data.src);
    });
    socket.on('message', function (data) {
        if (isObject(data.text)) {
            if (data.text.hasOwnProperty('stack')) msg.innerHTML = data.text.stack;
            else if (data.text.hasOwnProperty('message')) msg.innerHTML = data.text.message;
        } else msg.innerHTML = data.text;
    });
    socket.on('ready', (data) => {
        imagediv.remove();
        btnstart.remove();
    })
JS,$this::POS_END);
if($whatsapp !== false) {
    $this->registerJs(<<<JS
        socket.on('connect', (data) => {
            msg.innerHTML = 'Preparing ...';
            socket.emit('create-session',{ id: idlogin, text: 'Restoring Session' })
        });
    JS,$this::POS_END);
} else {
    $this->registerJs(<<<JS
        socket.on('connect', (data) => {
            msg.innerHTML = 'Ready To Run Whatsapp';
            btnstart.setAttribute('disabled',false);
        });
        btnstart.addEventListener("click", function(event) { 
            socket.emit('create-session',{ id: idlogin })
        });
    JS,$this::POS_END);    
}
?>