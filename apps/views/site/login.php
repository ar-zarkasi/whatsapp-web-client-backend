<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\bootstrap5\ActiveForm;
use yii\bootstrap5\Html;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="text-center mt-4">
    <h1 class="h2 h2-username">Welcome To Whatsapp Bot</h1>
    <p class="lead">
        Sign in to your account to continue
    </p>
</div>

<div class="card">
    <div class="card-body">
        <div class="m-sm-4">
            <?php $form = ActiveForm::begin([
                'id'=>'login-form',
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"mb-3\">{input}{error}</div>\n",
                    'labelOptions' => ['class' => 'form-label'],
                ],
                'options'=>[
                    'autocomplete'=>'off',
                ]
            ]); ?>
                <div class="mb-3">
                    <?= $form->field($model, 'username')->textInput([
                        'placeholder'=>'Enter Your Email',
                        'id'=>'email',
                        'class'=>'form-control form-control-lg',
                    ]) ?>
                </div>
                <div class="mb-3">
                    <?= $form->field($model, 'password')->passwordInput([
                        'placeholder'=>'Enter Your Password',
                        'class'=>'form-control form-control-lg',
                        'label'=>false,
                    ]) ?>
                </div>
                <div>
                    <?= $form->field($model, 'rememberMe')->checkbox([
                        'template' => "<label class=\"form-check\">{input}\n{label}\n{error}</label>",
                    ]) ?>
                </div>
                <div class="text-center mt-3">
                    <?= Html::submitButton('Sign In', ['class' => 'btn btn-lg btn-primary', 'name' => 'login-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
