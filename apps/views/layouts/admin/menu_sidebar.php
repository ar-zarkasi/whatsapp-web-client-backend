<?php
use yii\widgets\Menu;
use yii\helpers\Url;
use app\helpers\MenuBackend;
?>

<?=Menu::widget([
    'options'=>['class'=>'sidebar-nav'],
    'encodeLabels'=>false,
    'linkTemplate'=>'<a href="{url}" class="sidebar-link">{label}</a>',
    'activeCssClass'=>'active',
    'items'=>MenuBackend::menu(),
])?>
<?php
if(Yii::$app->user->identity->akses == 'root')
    echo Menu::widget([
        'options'=>['class'=>'sidebar-nav'],
        'encodeLabels'=>false,
        'activeCssClass'=>'active',
        'linkTemplate'=>'<a href="{url}" class="sidebar-link">{label}</a>',
        'items'=>MenuBackend::rootMenu(),
    ]);
?>
