<?php
use yii\bootstrap5\Html;
?>

<!-- Navigation-->
<nav class="navbar navbar-light bg-light static-top">
    <div class="container">
        <a class="navbar-brand" href="#">Techbot</a>
        <div class="d-flex justify-content-around p-1" role="group">
            <?=Html::a(Html::tag('i','',['class'=>'align-start','data-feather'=>'log-in']).' Login',['/login'],['class'=>'btn btn-outline-dark mx-3'])?>
            <?=Html::a('Daftar Sekarang','#',['class'=>'btn btn-primary'])?>
        </div>
    </div>
</nav>