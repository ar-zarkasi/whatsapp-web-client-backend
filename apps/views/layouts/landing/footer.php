<?php 
use yii\bootstrap5\Html;
?>
<!-- Footer-->
<footer class="footer bg-light">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 h-100 text-center text-lg-start my-auto">
                <ul class="list-inline mb-2">
                    <li class="list-inline-item"><a href="#!">About</a></li>
                    <li class="list-inline-item">⋅</li>
                    <li class="list-inline-item"><a href="#!">Contact</a></li>
                    <li class="list-inline-item">⋅</li>
                    <li class="list-inline-item"><a href="#!">Terms of Use</a></li>
                    <li class="list-inline-item">⋅</li>
                    <li class="list-inline-item"><a href="#!">Privacy Policy</a></li>
                </ul>
                <p class="text-muted small mb-4 mb-lg-0">&copy; Technergy Solutions 2021. All Rights Reserved.</p>
            </div>
            <div class="col-lg-6 h-100 text-center text-lg-end my-auto">
                <ul class="list-inline mb-0">
                    <li class="list-inline-item me-4">
                        <a href="#!"><i class="bi-facebook fs-3"></i> Technergy</a>
                    </li>
                    <li class="list-inline-item me-4">
                        <?=Html::a(Html::tag('i','',['class'=>'bi-whatsapp fs-3']).' Contact Us','https://wa.me/628523456123',['target'=>'_BLANK'])?>
                    </li>
                    <li class="list-inline-item">
                        <?=Html::a(Html::tag('i','',['class'=>'bi-instagram fs-3']).' @technergy.id','https://www.instagram.com/technergy.id',['target'=>'_BLANK'])?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>