<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LandingAsset extends AssetBundle
{
    public $basePath = '@webroot/landing-assets/';
    public $baseUrl = '@web/landing-assets/';
    public $css = [
        ['https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css','rel'=>'stylesheet','type'=>'text/css'],
        ['assets/favicon.ico','rel'=>'shortcut icon','type'=>'image/x-icon'],
        ['https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic','rel'=>'stylesheet','type'=>'text/css'],
        ['css/styles.css','rel'=>'stylesheet'],
    ];
    public $js = [
        ['https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js'],
        ['https://cdn.jsdelivr.net/npm/feather-icons/dist/feather.min.js'],
        'js/scripts.js',
        // ['https://cdn.startbootstrap.com/sb-forms-latest.js'],
    ];
    public $depends = [
        // 'yii\web\YiiAsset',
        // 'yii\bootstrap5\BootstrapAsset',
    ];
}
