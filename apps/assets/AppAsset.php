<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        ['https://fonts.gstatic.com','rel'=>'preconnect'],
        ['img/icons/icon-48x48.png','rel'=>'shortcut icon'],
        'css/app.css',
        ['https://fonts.googleapis.com/css2?family=Inter:wght@300;400;600&display=swap','rel'=>'stylesheet'],
        // 'https://use.fontawesome.com/releases/v5.3.1/css/all.css',
        ["https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css","rel"=>"stylesheet","integrity"=>"sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==","crossorigin"=>"anonymous","referrerpolicy"=>"no-referrer",],
    ];
    public $js = [
        'js/app.js',
        ['https://cdn.socket.io/4.3.2/socket.io.min.js','integrity'=>"sha384-KAZ4DtjNhLChOB/hxXuKqhMLYvx3b5MlT55xPEiNmREKRzeEm+RVPlTnAn0ajQNs",'crossorigin'=>"anonymous"],
        'js/start.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap5\BootstrapAsset',
    ];
}
