<?php

namespace app\modules\cms;

/**
 * backoffice module definition class
 */
class ControlPanel extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\cms\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
        $this->layout = '@app/views/layouts/main';
        // Sub Modules
        $this->modules = [];
    }
}
