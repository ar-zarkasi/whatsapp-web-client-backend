<?php
namespace app\modules\cms\models;

use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\bot\Chatbot;
use app\models\bot\ChatbotButton;
use app\models\bot\ChatbotList;

class ChatbotSearch extends Chatbot {
    public function rules()
    {
        return [
            [['id','id_user','level_name','all_days','limit_time','answered','regex','regex_value','bot_reply','bot_type','list_title','using_webhook','url_webhook','text_error_webhook','parent_id','active'],'safe'],
        ];
    }
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = self::find()->joinWith(['hari','waktu']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>['pageSize'=>25],
            'sort'=>['defaultOrder'=>['parent_id'=>SORT_ASC,'id'=>SORT_ASC]],
        ]);

        if (!$this->load($params) && !$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_user' => $this->id_user,
            'regex' => $this->regex,
            'using_webhook' => $this->using_webhook,
            'parent_id' => $this->parent_id,
            'regex' => $this->regex,
            'limit_time' => $this->limit_time,
            'all_days' => $this->all_days,
        ]);

        $query->andFilterWhere(['like', 'level_name', $this->level_name])
            ->andFilterWhere(['like', 'regex_value', $this->regex_value])
            ->andFilterWhere(['bot_reply'=>$this->bot_reply])
            ->andFilterWhere(['answered'=>$this->answered])
            ->andFilterWhere(['url_webhook'=>$this->url_webhook])
            ->andFilterWhere(['text_error_webhook'=>$this->text_error_webhook])
            ->andFilterWhere(['list_title'=>$this->list_title]);

        return $dataProvider;
    }
}