<?php

namespace app\modules\cms\controllers;
use yii;
use yii\web\Controller;
use app\models\bot\Chatbot;
use app\models\bot\ChatbotList;
use app\models\bot\ChatbotButton;
use app\models\bot\ChatbotTime;
use app\models\bot\ChatbotDays;
use app\modules\cms\models\ChatbotSearch;
use app\helpers\CustomModel;
use app\helpers\IndoFormat;
/**
 * Default controller for the `backoffice` module
 */
class ChatbotController extends Controller
{
    public function actionIndex()
    {
        $searchModel = new ChatbotSearch;
        $requestParams = Yii::$app->getRequest()->getBodyParams();
        if (empty($requestParams)) {
            $requestParams = Yii::$app->getRequest()->getQueryParams();
        }
        $dataProvider = $searchModel->search($requestParams);
        return $this->render('index',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Chatbot;
        $modelbtn = [new ChatbotButton];
        $modellist = [new ChatbotList];
        $modeltime = new ChatbotTime;
        // set default value
        $model->active = true;
        $model->all_days = true;
        $modeltime->time_from = '00:00';
        $modeltime->time_to = '23:59';
        $listactive = $model->queryRecursive();
        // load post
        $utama = $model->load(Yii::$app->request->post());
        if(Yii::$app->request->isPost && $utama) {
            $transaction = Yii::$app->db->beginTransaction();
            $valid = false;
            try {
                $model->id_user = Yii::$app->user->identity->id;
                $saveutama = $model->validate() && $model->save(false);
                if(!$saveutama) {
                    foreach($model->getErrors() as $idx => $error) {
                        $text = implode(", ",$error);
                        Yii::$app->session->addFlash('danger',$text);
                    }
                    throw new \yii\web\UnprocessableEntityHttpException("Validation Chatbot Error");
                } else $valid = true;

                // limit time
                if($model->limit_time) {
                    $modeltime->load(Yii::$app->request->post());
                    $modeltime->id_chatbot = $model->id;
                    $saveutama = $modeltime->validate() && $modeltime->save(false);
                    if(!$saveutama) {
                        foreach($modeltime->getErrors() as $idx => $error) {
                            $text = implode(", ",$error);
                            Yii::$app->session->addFlash('danger',$text);
                        }
                        throw new \yii\web\UnprocessableEntityHttpException("Validation Chatbot Error");
                    } else $valid = $valid && true;
                }
                // days
                if($model->all_days) {
                    $haris = Yii::$app->request->post('hari');
                    foreach($haris as $hari) {
                        $day = new ChatbotDays();
                        $day->id_chatbot = $model->id;
                        $day->days = $hari;
                        $day->text_en = IndoFormat::convertHari($hari,'en');
                        $day->text_id = IndoFormat::convertHari($hari,'id');
                        if(!$day->save()) {
                            foreach($modeltime->getErrors() as $idx => $error) {
                                $text = implode(", ",$error);
                                Yii::$app->session->addFlash('danger',$text);
                            }
                            throw new \yii\web\UnprocessableEntityHttpException("Validation Chatbot Daily Error");
                        }
                    }
                }
                // load multiple model
                if($model->bot_type == 1){
                    $modellist = CustomModel::createMultiple(ChatbotList::class,$modellist);
                    CustomModel::loadMultiple($modellist,Yii::$app->request->post());
                    // insert id chatbot
                    foreach ($modellist as $list) {
                        $list->id_chatbot = $model->id;
                    }
                    $valid = $valid && CustomModel::validateMultiple($modellist);
                    if($valid){
                        foreach ($modellist as $key => $list) {
                            if(!$list->save(false)) {
                                foreach($list->getErrors() as $idx => $error) {
                                    $text = implode(", ",$error);
                                    Yii::$app->session->addFlash('danger',$text);
                                }
                                throw new \yii\web\HttpException(422,"Validation Error Data List");
                            }
                        }
                    }
                } else if($model->bot_type == 2) {
                    $modelbtn = CustomModel::createMultiple(ChatbotButton::class,$$modelbtn);
                    CustomModel::loadMultiple($modelbtn,Yii::$app->request->post());
                    // insert id chatbot
                    foreach ($modelbtn as $btn) {
                        $btn->id_chatbot = $model->id;
                    }
                    $valid = $valid && CustomModel::validateMultiple($modelbtn);
                    if($valid){
                        foreach ($modelbtn as $key => $btn) {
                            if(!$btn->save(false)) {
                                foreach($btn->getErrors() as $idx => $error) {
                                    $text = implode(", ",$error);
                                    Yii::$app->session->addFlash('danger',$text);
                                }
                                throw new \yii\web\HttpException(422,"Validation Error Data List");
                            }
                        }
                    }
                }
                if($valid) {
                    $transaction->commit();
                    return $this->redirect(['index']);
                } else {
                    throw new \yii\web\HttpException(400,"Unknown Error");
                }
            } catch (\Throwable $th) {
                //throw $th;
                $transaction->rollBack();
            }


        }

        return $this->render('create',[
            'createdform'=>$model,
            'list'=>$modellist,
            'databutton'=>$modelbtn,
            'time'=>$modeltime,
            'listchatbot'=>$listactive,
        ]);
    }
}
?>