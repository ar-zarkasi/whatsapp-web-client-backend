<?php

namespace app\modules\cms\controllers;
use yii;
use yii\web\Controller;
use app\models\Setup;
use app\models\credential\UserSettings;
/**
 * Default controller for the `backoffice` module
 */
class SetupController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $user = Yii::$app->user->identity;
        $server = $_ENV['SERVER_SOCKET'];
        return $this->render('index',[
            'user'=>$user,
            'server'=>$server,
        ]);
    }
    public function actionLoad($id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $m = $this->findUserByToken($id);
        $wa = $m->credentialWhatsapp;
        $view = $this->renderPartial('data', ['wadata'=>$wa]);
        return [
            'layout'=>$view,
            'status'=>200,
            'message'=>'success',
        ];
    }
    public function actionWebhook()
    {
        $model = Yii::$app->user->identity->setting;
        if(empty($model)) {
            $model = new UserSettings;
            $model->user_id = Yii::$app->user->identity->id;
            $model->save();
        }
        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post()) && $model->validate()) {
            if($model->save()) Yii::$app->session->addFlash('success','Sukses Menyimpan Pengaturan Anda');
            else Yii::$app->session->addFlash('danger','Gagal Menyimpan Pengaturan!');
        }

        return $this->render('webhook',['model'=>$model]);
    }
    
    public function findUserByToken($token)
    {
        $user = \app\models\credential\Users::findIdentityByAccessToken($token);
        if(empty($user)) throw new \yii\web\NotFoundHttpException("User Tidak Ditemukan");
        return $user;
    }
}
