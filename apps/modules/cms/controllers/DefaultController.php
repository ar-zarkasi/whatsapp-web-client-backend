<?php

namespace app\modules\cms\controllers;
use yii;
use yii\web\Controller;

/**
 * Default controller for the `backoffice` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $user = Yii::$app->user->identity;
        return $this->render('index',[
            'user'=>$user,
        ]);
    }
}
