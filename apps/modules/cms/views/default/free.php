<div class="row">
    <div class="col-12">
        <div class="card border-top border-bottom border-end border-danger">
            <div class="card-body">
                <div class="row">
                    <div class="col mt-0">
                        <span class="text-muted">You Are Registered of Free Trial User</span>
                    </div>
                    <div class="col-auto">Remaining <strong class="remaining-time">00:00:00</strong></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php 
$subs = $user->subscribe;
if(!empty($subs)) {
    $expired = new \DateTime($subs->expired);
    $expired = $expired->format('M j, Y H:i:s');
    $this->registerJs(<<<JS
    const el = document.getElementsByClassName('remaining-time')[0];
    const waktu = "$expired";
    let timer = setInterval(countdown(el, waktu), 1000);
    JS,$this::POS_END);
}
?>