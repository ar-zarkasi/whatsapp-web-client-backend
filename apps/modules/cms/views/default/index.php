<?php 
$this->title = 'Dashboard';

if($user->akses === 'root') {
    echo $this->render('pay',['user'=>$user]);
}
else {
    if(empty($user->subscribe)) 
        echo $this->render('free',['user'=>$user]);
    else echo $this->render('pay',['user'=>$user]);
}
?>
