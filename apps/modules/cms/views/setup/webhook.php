<?php
use yii\bootstrap5\Html;
use yii\bootstrap5\ActiveForm;

$this->title = "Pengaturan Fitur";
?>
<div class="card">
    <div class="card-body">
        <?php $form = ActiveForm::begin(); ?>
        <div class="row g-0">
            <div class="col-12">
                <?=$form->field($model,'api')->checkbox()?>
            </div>
            <div class="col-12">
                <?=$form->field($model,'webhook')->checkbox()?>
                <?=$form->field($model,'webhook_url')->textInput(['placeholder'=>'URL Webhook'])?>
            </div>
            <div class="col-12">
                <div class="text-end">
                    <?=Html::submitButton('SIMPAN',['class'=>'btn btn-outline-success'])?>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>