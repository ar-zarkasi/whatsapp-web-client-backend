<?php
use yii\bootstrap5\Html;
use yii\helpers\Url;

$this->title = "Pengaturan Akun Whatsapp";
$login = $user->getEncodeToken();
$whatsapp = $user->credentialWhatsapp;
$socket = $server;
$load_data_uri = Url::to(['load'], $schema = true);
$this->registerCss(<<<CSS
.feather-big{
    width: 100%;
    height: auto;
}

CSS);
?>
<div class="row">
    <div class="col-12 col-md-3">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h5 class="card-title">
                            Credential Whatsapp
                        </h5>
                    </div>
                </div>
                <?=Html::img('#',['class'=>'img-fluid d-none','id'=>'image-qr'])?>
                <p class="fs-2" id="icon-ot"><?=Html::tag('i','',['data-feather'=>'key','class'=>'feather-big'])?></p>
            </div>
            <div class="card-footer">
                <span class="lead" id="message-log"></span>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-9">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h5 class="card-title">Account Information</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table class="table table-hover align-middle" id="table-information">
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="text-right">
                            <?=Html::button('Sign Out From Whatsapp',['class'=>'btn btn-danger btn-md d-none','id'=>'body-information'])?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php 
$this->registerJs(<<<JS
    const idlogin = "$socket"+'/'+"$login";
    const socket = io(idlogin);
    const msg = document.getElementById('message-log');
    const imagediv = document.getElementById('image-qr');
    const icon = document.getElementById('icon-ot');
    const btnout = document.getElementById('body-information');
    const tabdata = document.getElementById('table-information');

    btnout.addEventListener('click', (ev) => {
        const ids= ev.target.getAttribute('data-id');
        if(ids !== '' || (typeof ids !== 'undefined')) {
            console.log('logout',ids)
            socket.emit('log-out-wa', {id: ids});
            ev.target.classList.add('d-none')
        } else console.info('not-connect')
    })
    function hilang() {
        imagediv.classList.add('d-none')
        icon.classList.remove('d-none')
    }
    const cleartable = () => {
        btnout.setAttribute('data-id','')
        btnout.classList.add('d-none')
        const tbody = tabdata.querySelectorAll('tbody')[0]
        tbody.innerHTML = '';
    }
    function load_table(data) {
        cleartable()
        if(Object.keys(data).length > 0) {
            btnout.setAttribute('data-id',data.id)
            delete data.id;
            delete data.status;
            delete data.message;
            Object.entries(data).forEach(([key, val]) => {
                const rowCount = tabdata.rows.length;
                const row = tabdata.insertRow(rowCount);
                let cell = row.insertCell(0)
                cell.innerHTML = key
                cell = row.insertCell(1)
                cell.innerHTML = val
            });
            btnout.classList.remove('d-none')
        }
        return false;
    }
    socket.on('connect_error',(data) => {
        console.log('Error Not Connected');
        const server = io("$socket");
        server.emit('create-user-socket',{id: idlogin});
        server.on('success-creating',(sck) => {
            window.location.reload();
        })
    })
    socket.on('disconnect',() => {
        msg.innerHTML = "Whatsapp Server Not Ready";
        hilang()
        cleartable()
    });
    socket.on('disconnect-wa',() => {
        cleartable()
        socket.emit('create-session',{ id: idlogin, text: 'Creating Login' })
    })
    socket.on('qr', (data) => {
        console.log("Data QR Received", data);
        cleartable()
        imagediv.setAttribute('src', data.src);
        imagediv.classList.remove('d-none')
        icon.classList.add('d-none')
    });
    socket.on('message', function (data) {
        if (isObject(data.text)) {
            if (data.text.hasOwnProperty('stack')) msg.innerHTML = data.text.stack;
            else if (data.text.hasOwnProperty('message')) msg.innerHTML = data.text.message;
        } else msg.innerHTML = data.text;
    });
    socket.on('ready', (data) => {
        hilang()
        load_table(data)
    })
    function load_data(id) {
        const uri = "$load_data_uri"+"?id="+id
        console.log(uri)
    }
JS,$this::POS_END);
if($whatsapp !== false && $whatsapp != null) {
    $this->registerJs(<<<JS
        socket.on('connect', (data) => {
            msg.innerHTML = 'Preparing ...';
            socket.emit('create-session',{ id: idlogin, text: 'Restoring Session' })
        });
    JS,$this::POS_END);
} else {
    $this->registerJs(<<<JS
        socket.on('connect', (data) => {
            msg.innerHTML = 'Ready To Run Whatsapp';
        });
        socket.emit('create-session',{ id: idlogin, text: 'Creating Login' })
    JS,$this::POS_END);    
}
?>