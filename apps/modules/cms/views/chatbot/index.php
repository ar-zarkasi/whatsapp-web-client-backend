<?php
use yii\bootstrap5\Html;
use hscstudio\mimin\components\Mimin;
use kartik\grid\GridView;

$this->title = 'Techbot Chat - Chatbot Whatsapp';
?>

<h4 class="card-title">Chatbot Responder</h4>
<div class="row mt-4">
    <div class="col-12 mb-3">
        <?php if ((Mimin::checkRoute($this->context->id.'/create'))){
            echo Html::a('New', ['create'], ['class' => 'btn btn-outline-success']);
        }?>
    </div>
    <div class="col-12">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'emptyText'=>'&nbsp;',
            'pjax'=>true,
            'pjaxSettings'=>[
                'options'=>[
                    'id'=>'chatbot-page',
                    'enableReplaceState'=>false,
                    'enablePushState'=>false
                ],
            ],
            'columns' => [
                ['class' => 'kartik\grid\SerialColumn'],
                [
                    'attribute'=>'active',
                    'filterType'=>GridView::FILTER_SELECT2,
                    'filterWidgetOptions'=>[
                        'data'=>[0=>'Not Active',1=>'Active'],
                        'options'=>['prompt'=>'',],
                        'pluginOptions'=>['allowClear'=>true],
                    ],
                    'format'=>'raw',
                    'value'=>function($row){
                        return Html::checkbox('activestate-'.$row->id, $row->active,['class'=>'disabled-checkbox']);
                    }
                ],
                [
                    'attribute'=>'limit_time',
                    'format'=>'raw',
                    'value'=>function($row){
                        if($row->waktu == '') return '00:00 - 23:59';
                        else return $row->waktu->time_from.' - '.$row->waktu->time_to;
                    }
                ],
                [
                    'label'=>'user_messages',
                    'format'=>'raw',
                    'value'=>function($row){
                        if($row->regex) return $row->regex_value;
                        else return $row->answered;
                    }
                ],
                [
                    'attribute'=>'bot_reply',
                    'format'=>'raw',
                    'value'=>function($row){
                        if($row->using_webhook) return Html::tag('strong','Using Webhook to URL: '.$row->url_webhook);
                        else return substr(strip_tags($row->bot_reply),0,120);
                    }
                ],
                [
                    'class' => 'kartik\grid\ActionColumn',
                ],
            ]
        ]);?>
    </div>
</div>
<?php $this->registerJs(<<<JS
const getcheckboxTable = () => {
    const checkboxtable = document.getElementsByClassName('disabled-checkbox');
    for (let index = 0; index < checkboxtable.length; index++) {
        const semula = checkboxtable[index].checked
        checkboxtable[index].addEventListener('change',(e)=>{
            e.target.checked = semula        
        })
    }
}
getcheckboxTable(); // init
$('#chatbot-page').on('pjax:complete',() => {
    getcheckboxTable();
})
JS,$this::POS_END);?>