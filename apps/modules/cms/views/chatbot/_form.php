<?php
use yii\bootstrap5\Html;
use app\helpers\IndoFormat;
use bs\Flatpickr\FlatpickrWidget;
use wbraganca\dynamicform\DynamicFormWidget;
?>
<?=Html::activeHiddenInput($model,'id_user')?>
<div class="row">
    <div class="col-6">
        <?=$form->field($model,'active')->checkbox()?>
    </div>
    <div class="col-6">
        <?=$form->field($model,'parent_id')->dropdownList($listchatbot,['prompt'=>'....'])?>
    </div>
    <div class="col-12">
        <div class="row">
            <div class="col-2">
                <?=$form->field($model,'all_days')->checkbox()?>
            </div>
            <?=Html::checkboxList('hari', null, Indoformat::listHari(),['class'=>'col-12 col-md-10 px-0','encode'=>false,
                'item'=>function ($index, $label, $name, $checked, $value) {
                    $id = str_replace("[]","",$name).'-'.$index;
                    return "<div class=\"form-check form-check-inline\">
                    <input class=\"form-check-input\" type=\"checkbox\" name=\"$name\" id=\"$id\" value=\"$value\" ".($checked ? 'checked="checked"':'').">
                    <label class=\"form-check-label\" for=\"$id\">$label</label>
                  </div>";
                }
            ])?>
        </div>
    </div>
    <div class="col-12 mb-3">
        <div class="row g-0">
            <div class="col-2">
                <?=$form->field($model,'limit_time')->checkbox()?>
            </div>
            <div class="col-10 px-0">
                <?=!$time->isNewRecord ? Html::activeHiddenInput($time,'id_chatbot'):''?>
                <div class="row">
                    <div class="col-6">
                        <?=$form->field($time,'time_from')->widget(FlatpickrWidget::class,[
                            'options'=>['class'=>'form-control','id'=>'time_from'],
                            'clientOptions'=>[
                                'allowInput'=>true,
                                'enableTime'=>true,
                                'noCalendar'=>true,
                                'dateFormat'=>"H:i",
                                'time_24hr'=>true,
                                // 'disableMobile'=>"true",
                            ],
                        ])->label(false)?>
                    </div>
                    <div class="col-6">
                    <?=$form->field($time,'time_to')->widget(FlatpickrWidget::class,[
                            'options'=>['class'=>'form-control','id'=>'time_to'],
                            'clientOptions'=>[
                                'allowInput'=>true,
                                'enableTime'=>true,
                                'noCalendar'=>true,
                                'dateFormat'=>"H:i",
                                'time_24hr'=>true,
                                // 'disableMobile'=>"true",
                            ],
                        ])->label(false)?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 mb-3">
        <div class="row">
            <div class="col-6">
                <?=$form->field($model,'level_name')->textInput(['options'=>['class'=>'form-control mb-0']])?>
            </div>
            <div class="col-4 py-3">
                <span class="hint text-muted">**Gunakan <strong>greeting</strong> untuk chat pembuka</span>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="row">
            <div class="col-2">
                <?=$form->field($model,'regex')->checkbox()?>
            </div>
            <div class="col-10">
                <div class="row">
                    <div class="col-12">
                        <?=$form->field($model,'regex_value')->textInput(['placeholder'=>'Regex Pattern Jawaban Pengguna'])->label(false)?>
                    </div>
                    <div class="col-12">
                        <?=$form->field($model,'answered')->textInput(['placeholder'=>'Jawaban Pengguna'])->label(false)?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="row">
            <div class="col-2">
                <?=$form->field($model,'using_webhook')->checkbox()?>
            </div>
            <div class="col-10">
                <div class="row">
                    <div class="col-12">
                        <?=$form->field($model,'url_webhook')->textInput(['placeholder'=>'https://......'])->label(false)?>
                    </div>
                    <div class="col-12">
                        <?=$form->field($model,'text_error_webhook')->textInput(['placeholder'=>'Jawaban Jika Error Webhook'])->label(false)?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12">
        <?=$form->field($model,'bot_type')->dropdownList(
            $model->listType(),['prompt'=>'Pilih ...']
        )?>
        <?=$form->field($model,'list_title')->textInput(['placeholder'=>'Judul List'])?>
    </div>
    <div class="col-12">
        <?=$form->field($model,'bot_reply')->textarea(['rows'=>6])?>
    </div>
</div>
<?php DynamicFormWidget::begin([
    'widgetContainer' => 'wrapper_button_chat',
    'widgetBody' => '.form-button-chat',
    'widgetItem' => '.form-button-item',
    'min' => 1,
    'insertButton' => '.add-cbtn',
    'deleteButton' => '.delete-cbtn',
    'model' => $button[0],
    'formId' => $form->id,
    'formFields' => [
        'body',
        'kata'
    ],
]);?>
<h5 class="font-weight-bold">As Button</h5>
<div class="row gx-0 form-button-chat">
    <?php foreach ($button as $key => $btn):?>
    <div class="col-12 form-button-item mb-3">
        <div class="row">
            <?=Html::activeHiddenInput($btn,"[{$key}]id")?>
            <?=Html::activeHiddenInput($btn,"[{$key}]id_chatbot")?>
            <div class="col-12 col-sm-5">
                <?=$form->field($btn,"[{$key}]body")->textInput()?>
            </div>
            <div class="col-12 col-sm-5">
                <?=$form->field($btn,"[{$key}]kata")->textInput()?>
            </div>
            <div class="col-12 col-sm-2 py-sm-4 text-center">
                <div class="btn-group btn-group-sm">
                    <?=Html::button('<i class="fas fa-plus-circle"></i>',['class'=>'btn btn-primary add-cbtn'])?>
                    <?=Html::button('<i class="fas fa-trash-alt"></i>',['class'=>'btn btn-danger delete-cbtn'])?>
                </div>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
</div>
<?php DynamicFormWidget::end(); ?>
<hr />
<?php DynamicFormWidget::begin([
    'widgetContainer' => 'wrapper_list_chat',
    'widgetBody' => '.form-list-chat',
    'widgetItem' => '.form-list-item',
    'min' => 1,
    'insertButton' => '.add-clist',
    'deleteButton' => '.delete-clist',
    'model' => $list[0],
    'formId' => $form->id,
    'formFields' => [
        'body',
        'kata'
    ],
]);?>
<h5 class="font-weight-bold">As List</h5>
<div class="row gx-0 form-list-chat">
    <?php foreach ($list as $key => $ls):?>
    <div class="col-12 form-list-item mb-3">
        <div class="row">
            <?=Html::activeHiddenInput($ls,"[{$key}]id")?>
            <?=Html::activeHiddenInput($ls,"[{$key}]id_chatbot")?>
            <div class="col-12 col-sm-5">
                <?=$form->field($ls,"[{$key}]title")->textInput()?>
            </div>
            <div class="col-12 col-sm-5">
                <?=$form->field($ls,"[{$key}]description")->textInput()?>
            </div>
            <div class="col-12 col-sm-2 py-sm-4 text-center">
                <div class="btn-group btn-group-sm">
                    <?=Html::button('<i class="fas fa-plus-circle"></i>',['class'=>'btn btn-primary add-clist'])?>
                    <?=Html::button('<i class="fas fa-trash-alt"></i>',['class'=>'btn btn-danger delete-clist'])?>
                </div>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
</div>
<?php DynamicFormWidget::end(); ?>