<?php

use PHPUnit\Util\Log\JSON;
use yii\bootstrap5\ActiveForm;
use yii\bootstrap5\Html;

$this->title = "Create New Chatbot";
?>
<?php $form = ActiveForm::begin(['enableClientValidation'=>false,'options'=>['autocomplete'=>'off']]);?>
<div class="card">
    <div class="card-body">
        <h5 class="card-title">Specify Your New Chatbot</h5>
        <div class="row g-0 mt-4">
            <div class="col-12">
                <?= $this->render('_form', [
                    'model'=>$createdform,'form'=>$form,
                    'button'=>$databutton,'list'=>$list,
                    'time'=>$time,
                    'listchatbot'=>$listchatbot,
                ]) ?>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <?=Html::submitButton('Simpan Chatbot',['class'=>'btn btn-outline-success'])?>
    </div>
</div>
<?php ActiveForm::end(); 
$this->registerJs(<<<JS
    const listform = document.getElementsByClassName('wrapper_list_chat')[0];
    const butform = document.getElementsByClassName('wrapper_button_chat')[0];
    const tipe = document.getElementById('chatbot-bot_type');
    const regexcheck = document.getElementById('chatbot-regex');
    const limit = document.getElementById('chatbot-limit_time');
    const allday = document.getElementById('chatbot-all_days');
    const webhook = document.getElementById('chatbot-using_webhook');
    console.log(tipe);
    const hilangform = () => {
        const text = document.getElementsByClassName('field-chatbot-bot_reply')[0];
        const list = document.getElementsByClassName('field-chatbot-list_title')[0];
        listform.classList.add('d-none');
        butform.classList.add('d-none');
        text.classList.add('d-none');
        list.classList.add('d-none');
        if (tipe.value.toLowerCase() === '0') {
            text.classList.remove('d-none')
        } else if (tipe.value.toLowerCase() === '1') {
            listform.classList.remove('d-none')
            list.classList.remove('d-none')
        } else if (tipe.value.toLowerCase() === '2') {
            butform.classList.remove('d-none');
        }
    }
    const typeRegex = () => {
        const fieldregex = document.getElementsByClassName('field-chatbot-regex_value')[0];
        const fieldtext = document.getElementsByClassName('field-chatbot-answered')[0];
        fieldregex.classList.add('d-none')
        fieldtext.classList.add('d-none')
        if (regexcheck.checked === true){
            fieldregex.classList.remove('d-none')
            document.getElementById('chatbot-answered').value = '';
        } else {
            fieldtext.classList.remove('d-none')
            document.getElementById('chatbot-regex_value').value = '';
        }
    }
    const timecheck = () => {
        const from = document.getElementById('time_from');
        const to = document.getElementById('time_to');
        if(limit.checked === true) {
            from.classList.remove('disabled');
            to.classList.remove('disabled');
            from.removeAttribute("disabled");
            to.removeAttribute("disabled");
        } else {
            from.classList.add('disabled');
            to.classList.add('disabled');
            from.setAttribute("disabled","disabled");
            to.setAttribute("disabled","disabled");
        }
    }
    const daycheck = () => {
        const allcheckboxes = document.querySelectorAll("[name='hari[]']");
        const kondisi = allday.checked;
        for (let index = 0; index < allcheckboxes.length; index++) {
            allcheckboxes[index].checked = !kondisi;
            if(kondisi) {
                allcheckboxes[index].setAttribute('disabled','disabled')
            } else {
                allcheckboxes[index].removeAttribute('disabled')
            }
        }
    }
    
    const webhookcheck = () => {
        const d = document.getElementById('chatbot-url_webhook');
        const e = document.getElementById('chatbot-text_error_webhook');
        if(webhook.checked) {
            d.removeAttribute('disabled');
            e.removeAttribute('disabled');
        } else {
            d.value = '';
            e.value = '';
            d.setAttribute('disabled','disabled');
            e.setAttribute('disabled','disabled');

        }
    }


    tipe.addEventListener("change",hilangform)
    regexcheck.addEventListener("change",typeRegex)
    limit.addEventListener("change", timecheck)
    allday.addEventListener("change", daycheck)
    webhook.addEventListener("change",webhookcheck)

    typeRegex();
    hilangform();
    timecheck();
    daycheck();
    webhookcheck();
JS,$this::POS_END);?>