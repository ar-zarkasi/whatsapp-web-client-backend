<?php

namespace app\modules\root;

/**
 * root module definition class
 */
class ModuleAdmin extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\root\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
        $this->layout = '@app/views/layouts/main';
        // Sub Modules
        $this->modules = [
            'mimin' => [
                // 'class' => 'mdm\admin\Module',
                'class' => '\hscstudio\mimin\Module',
            ],
        ];
        
    }
}
