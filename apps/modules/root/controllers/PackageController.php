<?php

namespace app\modules\root\controllers;
use Yii;
use yii\web\Controller;
use app\models\Package;
use app\modules\root\models\PackageSearch;
/**
 * Default controller for the `root` module
 */
class PackageController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new PackageSearch;
        $requestParams = Yii::$app->getRequest()->getBodyParams();
        if (empty($requestParams)) {
            $requestParams = Yii::$app->getRequest()->getQueryParams();
        }
        $dataProvider = $searchModel->search($requestParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
        return $this->render('index');
    }

    public function actionCreate()
    {
        $model = new Package;
        $tr = Yii::$app->db->beginTransaction();
        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post()) && $model->validate()) {
            try {
                $model->save();
                Yii::$app->session->addFlash('success','Successfully Save A Package');
                $tr->commit();
                return $this->redirect('/root/package');
            } catch (\Throwable $th) {
                Yii::$app->session->addFlash('danger','Error When Saving Package');
                $tr->rollBack();
            }
        } else if($model->hasErrors()) {
            Yii::$app->session->addFlash('danger','Error When Save A Package');
            $tr->rollBack();
        }
        $title = "Buat Harga Paket";
        return $this->render('form',['model'=>$model,'title'=>$title]);
    }

    public function actionUpdate($id)
    {
        $model = Package::findOne($id);
        if(empty($model)) throw new \yii\web\HttpException(404,'Data Tidak Ditemukan');
        $tr = Yii::$app->db->beginTransaction();
        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post()) && $model->validate()) {
            try {
                $model->save();
                Yii::$app->session->addFlash('success','Successfully Save A Package');
                $tr->commit();
                return $this->redirect('/root/package');
            } catch (\Throwable $th) {
                Yii::$app->session->addFlash('danger','Error When Updating Package');
                $tr->rollBack();
            }
        } else if($model->hasErrors()) {
            Yii::$app->session->addFlash('danger','Error When Save A Package');
            $tr->rollBack();
        }
        $title = 'Update Harga Paket '.$model->packagename;
        return $this->render('form',['model'=>$model,'title'=>$title]);
    }

    public function actionDelete($id)
    {
        $model = Package::findOne($id);
        if(empty($model)) throw new \yii\web\HttpException(404,'Data Tidak Ditemukan');

        $tr = Yii::$app->db->beginTransaction();
        try {
            $model->delete();
            $tr->commit();
            return $this->redirect(['/root/package']);
        } catch (\Throwable $th) {
            $tr->rollBack();
            throw $th;
        }
    }
}
