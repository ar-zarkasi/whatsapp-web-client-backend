<?php

namespace app\modules\root\controllers;

use yii\web\Controller;
use yii\base\Model;
use app\models\Setup;
use Yii;
use yii\helpers\Url;
/**
 * Default controller for the `root` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $serverwa = $_ENV['SOCKET_SERVER'];
        return $this->render('index',['server'=>$serverwa]);
    }

    public function actionSetup()
    {
        $model = Setup::find()->where(['<>','group','setting'])->all();
        $nmodel = new Setup;
        $nmodel->setScenario('create');
        return $this->render('setup',[
            'model'=>$model,
            'nmodel'=> $nmodel,
        ]);
    }
    public function actionSaveSetup($id = null) {
        if(is_null($id)) {
            $model = new Setup;
            $model->scenario = 'create';
        } else {
            $model = Setup::findOne($id);
            if(empty($model)) throw new \yii\web\NotFoundHttpException("Data Pengaturan Tidak Ditemukan");
            $model->scenario = 'update';
        }
        $tr = Yii::$app->db->beginTransaction();
        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->addFlash('success','Sukses Menyimpan Pengaturan '.$model->name.'('.$model->group.') Berhasil');
            $tr->commit();
        } else {
            $tr->rollBack();
            Yii::$app->session->addFlash('error','Error! Terjadi kegagalan saat menyimpan semua pengaturan');
        }
        return $this->redirect(['/root/default/setup']);
    }

    public function actionDelSetup($id)
    {
        $model = Setup::findOne($id);
        if(empty($model)) throw new \yii\web\NotFoundHttpException("Data Pengaturan Tidak Ditemukan");
        $name = $model->name.'('.$model->group.')';
        if(Yii::$app->request->isPost && $model->delete()) {
            Yii::$app->session->addFlash('succes','Berhasil Menghapus Pengaturan '.$name);
        } else Yii::$app->session->addFlash('danger','Gagal Menghapus Pengaturan '.$name);
        return $this->redirect(['/root/default/setup']);
    }
}
