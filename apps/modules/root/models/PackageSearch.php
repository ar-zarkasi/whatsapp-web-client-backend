<?php

namespace app\modules\root\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Package;

/**
 * BlogSearch represents the model behind the search form of `app\models\BlogsClass`.
 */
class PackageSearch extends Package
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['fitur', 'price', 'price_before', 'showcase', 'packagename','special',], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>['pageSize'=>10],
            'sort'=>['defaultOrder'=>['id'=>SORT_ASC,'showcase'=>SORT_DESC]],
        ]);

        if (!$this->load($params) && !$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'showcase' => $this->showcase,
            'special' => $this->special,
        ]);

        $query->andFilterWhere(['like', 'fitur', $this->fitur])
            ->andFilterWhere(['like', 'packagename', $this->packagename])
            ->andFilterWhere(['price'=>$this->price])
            ->andFilterWhere(['price_before'=>$this->price_before]);

        return $dataProvider;
    }
}
