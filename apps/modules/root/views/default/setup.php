<?php
use yii\helpers\Html;
use yii\bootstrap5\ActiveForm;

$this->title = 'Pengaturan Setup Aplikasi';
$last = count($model) - 1;
?>
<div class="card">
        <div class="card-body">
            <?php foreach($model as $key => $data): 
            if($data->not_deleted == 0) $collength = ['8','4'];
            else $collength = ['10','2'];
            ?>
            <?php $form = ActiveForm::begin(['action'=>['save-setup','id'=>$data->id]]); ?>
            <div class="row mb-3 mb-md-0">
                <?= Html::activeHiddenInput($data, "id"); ?>
                <div class="col-12 col-md-<?=$collength[0]?>">
                    <div class="row">
                        <div class="col-4">
                            <?=$form->field($data,"group")->textInput(['readonly'=>$data->not_deleted==1])?>
                        </div>
                        <div class="col-4">
                            <?=$form->field($data,"name")->textInput(['readonly'=>$data->not_deleted==1])?>
                        </div>
                        <div class="col-4">
                            <?=$form->field($data,"value")?>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-<?=$collength[1]?> mt-0 mt-sm-2">
                    <br/>
                    <div class="btn-group">
                        <?php
                            echo Html::submitButton(Html::tag('i','',['data-feather'=>'save']).' Update', ['class'=>'btn btn-primary']);
                            if($data->not_deleted == 0) echo Html::a(Html::tag('i','',['data-feather'=>'trash']).' Hapus', ['del-setup','id'=>$data->id], ['class'=>'btn btn-danger','data-method'=>'post']);
                        ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
            <?php endforeach; ?>
            <div class="divider" />
            <?php $form = ActiveForm::begin(['action'=>['save-setup'],'options'=>['autocomplete'=>'off']]); ?>
            <div class="row mb-3 mb-md-0">
                <div class="col-12 col-md-10">
                    <div class="row">
                        <div class="col-4">
                            <?=$form->field($nmodel,"group")->textInput()?>
                        </div>
                        <div class="col-4">
                            <?=$form->field($nmodel,"name")->textInput()?>
                        </div>
                        <div class="col-4">
                            <?=$form->field($nmodel,"value")?>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-2 mt-0 mt-sm-2">
                    <br/>
                    <div class="btn-group">
                        <?php
                            echo Html::submitButton(Html::tag('i','',['data-feather'=>'save']).' Tambah', ['class'=>'btn btn-success']);
                        ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
</div>