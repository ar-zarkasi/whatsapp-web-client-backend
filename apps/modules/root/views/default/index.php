<?php
use yii;
use yii\bootstrap5\Html;
$this->title = "Check Server WA State";
?>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-12"><h5 class="card-title">Jumlah Client Connected</h5></div>
            <div class="col-12">
                <p class="lead" id="cek-state"></p>
            </div>
        </div>
    </div>
</div>
<?php

$this->registerJs(<<<JS
    const server = io("$server");
    const pstate = document.getElementById('cek-state')
    server.emit('check-client-connected',{ server: true})
    server.on('answer-check-client',(data) => {
        console.log(data);
        pstate.innerHTML = '<b>' + data.jumlah + '</b> Client Connected'
    })
JS,$this::POS_END);