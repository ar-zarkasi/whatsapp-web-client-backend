<?php
use yii\bootstrap5\Html;
use yii\bootstrap5\ActiveForm;
use kartik\widgets\FileInput;
use yii\helpers\Url;
use kartik\number\NumberControl;
use dosamigos\tinymce\TinyMce;
use yii\web\JsExpression;


$this->title = $title;
?>
<?php $form = ActiveForm::begin(); ?>
<div class="card">
    <div class="card-body ma-0 pa-0">
        <div class="row g-0">
            <div class="col-12 col-sm-6">
                <?=$form->field($model,'packagename')?>
            </div>
            <div class="col-12">
                <div class="row">
                    <div class="col-12 col-sm-6">
                        <?=$form->field($model,'price')->widget(NumberControl::class,[
                            'maskedInputOptions'=>[
                                'allowMinus'=>false,
                            ]
                        ])?>
                    </div>
                    <div class="col-12 col-sm-6">
                        <?=$form->field($model,'price_before')->widget(NumberControl::class,[
                            'maskedInputOptions'=>[
                                'allowMinus'=>false,
                            ]
                        ])?>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <?=$form->field($model,'fitur')->widget(TinyMce::class,[
                    'options'=>['rows',6],
                    'language'=>'id',
                ])?>
            </div>
            <div class="col-12">
                <div class="row g-0">
                    <div class="col-12 col-sm-6">
                        <?=$form->field($model,'special')->checkbox()?>
                    </div>
                    <div class="col-12 col-sm-6">
                        <?=$form->field($model,'showcase')->checkbox()?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="text-end">
                <?= Html::a(Html::tag('i','',['data-feather'=>'x-circle']).' Batal', ['index'] ,['class' => 'btn btn-outline-secondary']) ?>
                <?= Html::submitButton(Html::tag('i','',['data-feather'=>'save']).' Simpan', ['class' => 'btn btn-outline-success']) ?>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end();?>
