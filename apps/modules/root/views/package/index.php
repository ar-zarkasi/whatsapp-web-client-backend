<?php
use yii\helpers\Html;
use hscstudio\mimin\components\Mimin;
use kartik\grid\GridView;

$this->title = 'Pricelist Harga Techbot';
?>
<div class="row">
    <div class="col-12">
        <div class="text-start mt-2 mb-3">
            <?php
            if (Mimin::checkRoute($this->context->id.'/create'))
                echo Html::a(Html::tag('i','',['class'=>'align-start','data-feather'=>'plus-square']).' Add New',['create'],['class'=>'btn btn-primary'])
            ?>
        </div>
    </div>
    <div class="col-12 col-md-6 fill-height"></div>
    <div class="col-12 mt-2 mb-2">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout'=>'
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-12">
                        {items}
                        <div class="row">
                            <div class="col-md-12">
                                {summary}{pager}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            ',
            'pjax'=>true,
            'pjaxSettings'=>[
                'options'=>[
                    'id'=>'packaget-table',
                    'enablePushState'=>false,
                    'enableReplaceState'=>false,
                ],
            ],
            'bordered'=>true,
            'responsive'=>false,'responsiveWrap'=>true,
            'columns' => [
                ['class' => 'kartik\grid\SerialColumn'],
                'id',
                'packagename',
                [
                    'attribute'=>'fitur',
                    'format'=>'html',
                ],
                [
                    'attribute'=>'price',
                    'format'=>'raw',
                    'value'=>function($one) { 
                        if($one->price_before > 0)
                            $p = Html::tag('p','Rp '.number_format($one->price).' '.Html::tag('del','Rp '.number_format($one->price_before))); 
                        else
                            $p = Html::tag('p','Rp '.number_format($one->price)); 
                        return $p;
                    },
                ],
                [
                    'attribute'=>'showcase',
                    'format'=>'html',
                    'value'=>function($one) {
                        if($one) return Html::tag('i','',['data-feather'=>'check-circle']);
                        return Html::tag('i','',['data-feather'=>'x-octagon']);
                    }
                ],
                [
                    'class' => 'kartik\grid\ActionColumn',
                    'visible'=>Mimin::checkRoute('update')||Mimin::checkRoute('delete'),
                    'template'=>Mimin::filterActionColumn(['update','delete'],$this->context->route),
                ],
            ],
        ]); ?>
    </div>
</div>