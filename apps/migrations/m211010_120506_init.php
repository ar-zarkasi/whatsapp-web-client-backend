<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Class m211010_120506_init
 */
class m211010_120506_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('session',[
            'id'=> $this->char(40)->notNull(),
            'expire'=>$this->integer(11)->unsigned(),
            'data'=>'LONGBLOB',
        ]);
        $this->addPrimaryKey('session_pk', 'session', 'id');
        $this->createTable('users', [
            'id' => Schema::TYPE_UPK,
            'email' => $this->string(320)->notNull(),
            'phone' => $this->string(14)->defaultValue(NULL),
            'confirmed_email' => $this->dateTime()->defaultValue(NULL), // NOT NULL = Confirmed, NULL = Not Confirmed
            'confirmed_phone' => $this->dateTime()->defaultValue(NULL), // NOT NULL = Confirmed, NULL = Not Confirmed
            'name' => $this->string(160)->notNull(),
            'auth' => $this->string(100)->notNull(),
            'token' => $this->string(125)->defaultValue(NULL),
            'password' => $this->text()->notNull(),
            'active' => $this->tinyInteger(1)->defaultValue(NULL),
            'parent_user'=>$this->integer(10)->unsigned()->defaultValue(NULL),
            'token_expiry' => $this->datetime()->defaultValue(NULL),
            'deleted' => $this->tinyInteger(1)->unsigned()->defaultValue(0),
            'created_date' => $this->dateTime()->defaultValue(NULL),
            'modified_date' => $this->timestamp().' ON UPDATE CURRENT_TIMESTAMP',
        ]);
        $this->createIndex('users_mail', 'users', 'email',false);
        $this->createIndex('users_phone', 'users', 'phone',false);
        try{
            $this->createIndex('users_unik', 'users', ['email','phone'],true);
        } catch(\Exception $e) {
            echo "users_unik index not implemented";
        }
        $this->createTable('package',[
            'id'=>Schema::TYPE_UPK,
            'fitur'=>$this->text()->defaultValue(NULL),
            'price'=>$this->double()->defaultValue(0),
            'packagename'=>$this->string(160)->notNull(),
            'special'=>$this->boolean()->defaultValue(false),
            'price_before'=>$this->double()->defaultValue(0),
            'showcase'=>$this->tinyInteger(1)->defaultValue(1),
        ]);
        $this->addCommentOnColumn('package', 'showcase', '0 = jangan tampilkan di umum/website, 1 = tampilkan di umum/website');
        $this->createTable('pay_user',[
            'id'=>Schema::TYPE_UPK,
            'id_user'=>$this->integer(10)->unsigned()->notNull(),
            'id_package'=>$this->integer(10)->unsigned()->notNull(),
            'payment'=>$this->double()->defaultValue(0),
            'payment_status'=>$this->tinyInteger(1)->defaultValue(0),
            'active'=>$this->tinyInteger(1)->defaultValue(0),
            'payment_date'=>$this->dateTime()->defaultValue(NULL),
            'data_package'=>$this->text()->defaultValue(NULL),
            'expired'=>$this->dateTime()->defaultValue(NULL),
            'jatuh_tempo'=>$this->dateTime()->defaultValue(NULL),
        ]);
        $this->createTable('pay_user_history',[
            'id'=>Schema::TYPE_UPK,
            'id_pay_user'=>$this->integer(10)->unsigned()->notNull(),
            'data_pay'=>$this->text()->defaultValue(NULL),
        ]);
        $this->createTable('setup', [
            'id' => Schema::TYPE_UPK,
            'group' => $this->string(14)->notNull(),
            'name'=> $this->string(320)->notNull(),
            'value'=> $this->string(320)->defaultValue(NULL),
            'not_deleted'=> $this->tinyInteger(1)->defaultValue(0), // 0 bisa di hapus, 1 = tidak bisa dihapus
            'modified_date' => $this->timestamp().' ON UPDATE CURRENT_TIMESTAMP',
        ]);
        $this->createIndex('grup_setup', 'setup', 'group',false);
        try{
            $this->createIndex('grup_setup_unique', 'setup', ['group','name'],true);
        } catch(\Exception $e) {
            echo "grup_setup_unique index not implemented";
        }
        $this->batchInsert('setup',['group','name','not_deleted'],[
            ['setting','path_logo',1],
            ['setting','filename_logo',1],
            ['setting','title',1],
            ['meta','title',1],
            ['meta','description',1],
            ['meta','keyword',1],
            ['midtrans','client',1],
            ['midtrans','server',1],
            ['midtrans','merchantid',1],
            ['midtrans','url',1],
            ['midtrans','url_js',1],
            ['mapbox','token',1],
            ['default','grup_user',1],
            ['default','subscribe',1],
            ['default','socket_url',1],
        ]); 
        $this->insert('setup',['group'=>'apis','name'=>'apikey','value'=>\Yii::$app->security->generateRandomString(32),'not_deleted'=>1]);

        $this->createTable('wa_id', [
            'id'=>Schema::TYPE_UPK,
            'id_user'=>$this->integer(10)->unsigned()->notNull(),
            'wa_session'=>$this->text()->defaultValue(NULL),
            'last_status'=>$this->string(120)->defaultValue(NULL),
            'last_connected'=>$this->dateTime()->defaultValue(NULL),
            'data'=>$this->text()->defaultValue(NULL),
        ]);
        $this->createIndex('wa_user_id', 'wa_id', 'id_user', false);
        try {
            $this->execute('ALTER TABLE `wa_id` ADD FULLTEXT INDEX `wa_user_session` (`wa_session`)');
        } catch (\Throwable $th) {
            echo "SQL server Does Not Support FULLTEXT INDEX";
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('session');
        $this->dropTable('users');
        $this->dropTable('setup');
        $this->dropTable('wa_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211010_120506_init cannot be reverted.\n";

        return false;
    }
    */
}
