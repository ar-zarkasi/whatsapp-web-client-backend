<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Class m211225_150801_chatbot
 */
class m211225_150801_chatbot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('chatbot',[
            'id'=>Schema::TYPE_UPK,
            'id_user'=>$this->integer()->unsigned()->notNull(),
            'level_name'=>$this->string(36)->defaultValue('greetings'),
            'all_days'=>$this->tinyInteger(1)->unsigned()->defaultValue(1),
            'limit_time'=>$this->tinyInteger(1)->unsigned()->defaultValue(0),
            'answered'=>$this->string(160)->defaultValue(NULL),
            'regex'=>$this->tinyInteger(1)->unsigned()->defaultValue(0),
            'regex_value'=>$this->string(160)->defaultValue(NULL),
            'bot_reply'=>$this->text()->defaultValue(NULL),
            'bot_type'=>$this->tinyInteger(1)->unsigned()->defaultValue(0),
            'list_title'=>$this->string(120)->defaultValue(NULL),
            'using_webhook'=>$this->tinyInteger(1)->unsigned()->defaultValue(0),
            'url_webhook'=>$this->text()->defaultValue(NULL),
            'text_error_webhook'=>$this->text()->defaultValue(NULL),
            'parent_id'=>$this->integer()->unsigned()->defaultValue(0),
            'active'=>$this->tinyInteger(1)->unsigned()->defaultValue(0),
        ]);
        $this->addCommentOnColumn('chatbot','bot_type','0 = text, 1 = list, 2 button');
        $this->createIndex('chatbot_parent','chatbot','parent_id',false);
        $this->createIndex('level_chat','chatbot','level_name',false);
        $this->createIndex('level_chat_parent','chatbot',['id','level_name','parent_id'],true);
        
        $this->createTable('chatbot_button',[
            'id'=>Schema::TYPE_UPK,
            'id_chatbot'=>$this->integer()->unsigned()->notNull(),
            'body'=>$this->string(120)->defaultValue(NULL),
            'kata'=>$this->text()->defaultValue(NULL),
        ]);
        $this->createIndex('idx_button_chatbot','chatbot_button','id_chatbot',false);
        $this->addForeignKey('foreign_btn_chat','chatbot_button', 'id_chatbot', 'chatbot', 'id', 'cascade', 'cascade');
        $this->createTable('chatbot_list',[
            'id'=>Schema::TYPE_UPK,
            'id_chatbot'=>$this->integer()->unsigned()->notNull(),
            'title'=>$this->string(160)->notNull(),
            'description'=>$this->text()->defaultValue(NULL),
        ]);
        $this->createIndex('idx_list_chatbot','chatbot_list','id_chatbot',false);
        $this->addForeignKey('foreign_list_chat','chatbot_list', 'id_chatbot', 'chatbot', 'id', 'cascade', 'cascade');

        $this->createTable('chatbot_days',[
            'id'=>Schema::TYPE_UPK,
            'id_chatbot'=>$this->integer()->unsigned()->notNull(),
            'days'=>$this->tinyInteger(1)->defaultValue(0),
            'text_en'=>$this->string(20)->defaultValue('Sunday'),
            'text_id'=>$this->string(20)->defaultValue('Minggu'),
        ]);
        $this->createIndex('hari_chatbot','chatbot_days','id_chatbot',false);
        $this->createIndex('hari_int_chatbot','chatbot_days','days',false);
        $this->createIndex('hari_int_unique_chatbot','chatbot_days',['id_chatbot','days'],true);

        $this->createTable('chatbot_time',[
            'id'=>Schema::TYPE_UPK,
            'id_chatbot'=>$this->integer()->unsigned()->notNull(),
            'time_from'=>$this->time()->notNull(),
            'time_to'=>$this->time()->notNull(),
        ]);
        $this->createIndex('hari_chatbot','chatbot_time','id_chatbot',false);

        $this->createTable('session_chatbot',[
            'id'=>Schema::TYPE_UPK,
            'time'=>$this->integer(15)->notNull(),
            'session_id'=>$this->text()->defaultValue(NULL),
            'phone'=>$this->string(18)->notNull(),
            'level_id'=>$this->integer()->unsigned()->notNull(),
            'level_name'=>$this->string(36)->defaultValue(NULL),
            'log_data'=>$this->text()->defaultValue(NULL),
            'route'=>$this->string(120)->defaultValue(0),
            'expired'=>$this->dateTime()->notNull(),
        ]);
        $this->createIndex('sesi_level_chat','session_chatbot','level_id',false);
        $this->createIndex('sesi_level_chat_cust','session_chatbot','phone',false);
        $this->createIndex('sesi_level_chat_exp','session_chatbot','expired',false);
        try {
            $this->createIndex('sesi_level_unique','session_chatbot',['phone','session_id'],false);
        } catch(\Exception $e) {
            echo "Your Database Server Not Support Index TEXT type";
        }

        $this->createTable('user_setting',[
            'id'=>Schema::TYPE_UPK,
            'user_id'=>$this->integer()->unsigned()->notNull(),
            'api'=>$this->tinyInteger(1)->unsigned()->defaultValue(0),
            'webhook'=>$this->tinyInteger(1)->unsigned()->defaultValue(0),
            'webhook_url'=>$this->text()->defaultValue(NULL),
        ]);
        $this->createIndex('idx_user_setting','user_setting','user_id',false);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('chatbot');
        $this->dropForeignKey('foreign_btn_chat', 'chatbot_button');
        $this->dropTable('chatbot_button');
        $this->dropForeignKey('foreign_list_chat', 'chatbot_list');
        $this->dropTable('chatbot_list');
        $this->dropTable('chatbot_days');
        $this->dropTable('chatbot_time');
        $this->dropTable('session_chatbot');
        $this->dropTable('user_setting');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211225_150801_chatbot cannot be reverted.\n";

        return false;
    }
    */
}
