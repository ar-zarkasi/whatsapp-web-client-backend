<?php

namespace app\models\bot;

use Yii;

/**
 * This is the model class for table "chatbot_list".
 *
 * @property int $id
 * @property int $id_chatbot
 * @property string $title
 * @property string|null $description
 *
 * @property Chatbot $chatbot
 */
class ChatbotList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'chatbot_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_chatbot', 'title'], 'required'],
            [['id_chatbot'], 'integer'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 160],
            [['id_chatbot'], 'exist', 'skipOnError' => true, 'targetClass' => Chatbot::class, 'targetAttribute' => ['id_chatbot' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_chatbot' => Yii::t('app', 'Id Chatbot'),
            'title' => Yii::t('app', 'Judul List'),
            'description' => Yii::t('app', 'Deskripsi'),
        ];
    }

    /**
     * Gets query for [[Chatbot]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChatbot()
    {
        return $this->hasOne(Chatbot::class, ['id' => 'id_chatbot']);
    }
}
