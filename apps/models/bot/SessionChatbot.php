<?php

namespace app\models\bot;

use Yii;

/**
 * This is the model class for table "session_chatbot".
 *
 * @property int $id
 * @property int $time
 * @property string|null $session_id
 * @property string $phone
 * @property int $level_id
 * @property string|null $level_name
 * @property string|null $log_data
 * @property string|null $route
 * @property string $expired
 */
class SessionChatbot extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'session_chatbot';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['time', 'phone', 'level_id', 'expired'], 'required'],
            [['time', 'level_id'], 'integer'],
            [['session_id', 'log_data'], 'string'],
            [['expired'], 'safe'],
            [['phone'], 'string', 'max' => 18],
            [['level_name'], 'string', 'max' => 36],
            [['route'], 'string', 'max' => 120],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'time' => Yii::t('app', 'Time'),
            'session_id' => Yii::t('app', 'Session ID'),
            'phone' => Yii::t('app', 'Phone'),
            'level_id' => Yii::t('app', 'Level ID'),
            'level_name' => Yii::t('app', 'Level Name'),
            'log_data' => Yii::t('app', 'Log Data'),
            'route' => Yii::t('app', 'Route'),
            'expired' => Yii::t('app', 'Expired'),
        ];
    }
}
