<?php

namespace app\models\bot;

use Yii;

/**
 * This is the model class for table "chatbot_days".
 *
 * @property int $id
 * @property int $id_chatbot
 * @property int|null $days
 * @property string|null $text_en
 * @property string|null $text_id
 */
class ChatbotDays extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'chatbot_days';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_chatbot','days'], 'required'],
            [['id_chatbot', 'days'], 'integer'],
            [['text_en', 'text_id'], 'string', 'max' => 20],
            [['id_chatbot', 'days'], 'unique', 'targetAttribute' => ['id_chatbot', 'days']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_chatbot' => Yii::t('app', 'Id Chatbot'),
            'days' => Yii::t('app', 'Days'),
            'text_en' => Yii::t('app', 'Text En'),
            'text_id' => Yii::t('app', 'Text ID'),
        ];
    }
}
