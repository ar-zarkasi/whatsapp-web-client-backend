<?php

namespace app\models\bot;

use Yii;
use app\models\bot\ChatbotButton;
use app\models\bot\ChatbotList;
use app\models\bot\ChatbotDays;
use app\models\bot\ChatbotTime;
/**
 * This is the model class for table "chatbot".
 *
 * @property int $id
 * @property int $id_user
 * @property string|null $level_name
 * @property int|null $all_days
 * @property int|null $limit_time
 * @property string|null $answered
 * @property int|null $regex
 * @property string|null $regex_value
 * @property string|null $bot_reply
 * @property int|null $bot_type 0 = text, 1 = list, 2 button
 * @property string|null $list_title
 * @property int|null $using_webhook
 * @property string|null $url_webhook
 * @property string|null $text_error_webhook
 * @property int|null $parent_id
 * @property int|null $active
 *
 * @property ChatbotButton[] $chatbotButtons
 * @property ChatbotList[] $chatbotLists
 */
class Chatbot extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'chatbot';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user','bot_type','level_name'], 'required'],
            [['id_user', 'all_days', 'limit_time', 'regex', 'bot_type', 'using_webhook', 'active'], 'integer'],
            [['bot_reply', 'url_webhook', 'text_error_webhook'], 'string'],
            [['answered'],'required','when'=>function($model){ return !$model->regex;}],
            [['regex_value'],'required','when'=>function($model){ return $model->regex;}],
            [['bot_reply'],'required','when'=>function($model){ return $model->bot_type == 0;}],
            [['list_title'],'required','when'=>function($model){ return $model->bot_type == 1;}],
            [['url_webhook','text_error_webhook'],'required','when'=>function($model){ return $model->using_webhook;}],
            [['level_name'], 'string', 'max' => 36],
            [['answered', 'regex_value'], 'string', 'max' => 160],
            [['list_title'], 'string', 'max' => 120],
            [['parent_id'],'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_user' => Yii::t('app', 'Id User'),
            'level_name' => Yii::t('app', 'Level Chat'),
            'all_days' => Yii::t('app', 'All Days'),
            'limit_time' => Yii::t('app', 'Time'),
            'answered' => Yii::t('app', 'Answered'),
            'regex' => Yii::t('app', 'Regex ?'),
            'regex_value' => Yii::t('app', 'Regex Pattern'),
            'bot_reply' => Yii::t('app', 'Jawaban Bot'),
            'bot_type' => Yii::t('app', 'Jenis Balasan'),
            'list_title' => Yii::t('app', 'List Title'),
            'using_webhook' => Yii::t('app', 'Gunakan Webhook'),
            'url_webhook' => Yii::t('app', 'Url Webhook'),
            'text_error_webhook' => Yii::t('app', 'Jawaban Error'),
            'parent_id' => Yii::t('app', 'Parent Chatbot'),
            'active' => Yii::t('app', 'Active'),
        ];
    }

    public function listType()
    {
        return [
            '0'=>'Text',
            '1'=>'List',
            '2'=>'Button',
        ];
    }

    public function queryRecursive($id = null, $parent = 0)
    {
        $arr = [];
        $m = self::find()->where(['active'=>1]);
        if(!Yii::$app->user->isGuest) {
            $m->andWhere(['id_user'=>Yii::$app->user->identity->id]);
        }
        if(!is_null($id)) $m->andWhere(['parent_id'=>$id]);
        else $m->andWhere(['is','parent_id', new \yii\db\Expression('NULL')]);
        foreach ($m->all() as $idx => $chatbot) {
            $text = '';
            for ($i=0; $i < $parent; $i++) { 
                $text .= '-';
            }
            if($parent > 0) $text .= ' ';
            $arr[$chatbot->id] = $text.$chatbot->level_name;
            // search child
            $child = $chatbot->queryRecursive($chatbot->id, $parent+1);
            if(count($child) > 0) {
                foreach ($child as $ids => $value) {
                    $arr[$ids] = $value;
                }
            }
        }
        return $arr;
    }

    /**
     * Gets query for [[ChatbotButtons]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChatbotButtons()
    {
        return $this->hasMany(ChatbotButton::class, ['id_chatbot' => 'id']);
    }

    /**
     * Gets query for [[ChatbotLists]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChatbotLists()
    {
        return $this->hasMany(ChatbotList::class, ['id_chatbot' => 'id']);
    }

    public function getHari()
    {
        return $this->hasMany(ChatbotDays::class,['id_chatbot'=>'id']);
    }

    public function getWaktu()
    {
        return $this->hasOne(ChatbotTime::class,['id_chatbot'=>'id']);
    }
}
