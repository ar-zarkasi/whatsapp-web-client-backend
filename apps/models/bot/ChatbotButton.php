<?php

namespace app\models\bot;

use Yii;

/**
 * This is the model class for table "chatbot_button".
 *
 * @property int $id
 * @property int $id_chatbot
 * @property string|null $body
 * @property string|null $kata
 *
 * @property Chatbot $chatbot
 */
class ChatbotButton extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'chatbot_button';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_chatbot','body','kata'], 'required'],
            [['id_chatbot'], 'integer'],
            [['kata'], 'string'],
            [['body'], 'string', 'max' => 120],
            [['id_chatbot'], 'exist', 'skipOnError' => true, 'targetClass' => Chatbot::class, 'targetAttribute' => ['id_chatbot' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_chatbot' => Yii::t('app', 'Id Chatbot'),
            'body' => Yii::t('app', 'Button Text'),
            'kata' => Yii::t('app', 'Deskripsi'),
        ];
    }

    /**
     * Gets query for [[Chatbot]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChatbot()
    {
        return $this->hasOne(Chatbot::class, ['id' => 'id_chatbot']);
    }
}
