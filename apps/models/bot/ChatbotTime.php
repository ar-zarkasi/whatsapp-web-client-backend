<?php

namespace app\models\bot;

use Yii;

/**
 * This is the model class for table "chatbot_time".
 *
 * @property int $id
 * @property int $id_chatbot
 * @property string $time_from
 * @property string $time_to
 */
class ChatbotTime extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'chatbot_time';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_chatbot', 'time_from', 'time_to'], 'required'],
            [['id_chatbot'], 'integer'],
            // [['time_from', 'time_to'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_chatbot' => Yii::t('app', 'Id Chatbot'),
            'time_from' => Yii::t('app', 'Time From'),
            'time_to' => Yii::t('app', 'Time To'),
        ];
    }
}
