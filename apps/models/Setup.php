<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "setup".
 *
 * @property int $id
 * @property string $group
 * @property string $name
 * @property string|null $value
 * @property int|null $not_deleted
 * @property string|null $modified_date
 */
class Setup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'setup';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['group', 'name'], 'required','on'=>['create','update']],
            [['not_deleted'], 'integer'],
            [['modified_date','id'], 'safe'],
            [['group'], 'string', 'max' => 14],
            [['name', 'value'], 'string', 'max' => 320],
            [['group', 'name'], 'unique', 'targetAttribute' => ['group', 'name'],'on'=>['create']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'group' => Yii::t('app', 'Group'),
            'name' => Yii::t('app', 'Name'),
            'value' => Yii::t('app', 'Value'),
            'not_deleted' => Yii::t('app', 'Not Deleted'),
            'modified_date' => Yii::t('app', 'Modified Date'),
        ];
    }

    public static function DefaultSetup()
    {
        $grup_user = self::find()->where(['group'=>'default','name'=>'grup_user'])->one()->value;
        $socket_url = self::find()->where(['group'=>'default','name'=>'socket_url'])->one()->value;
        $subscribe = self::find()->where(['group'=>'default','name'=>'subscribe'])->one()->value;
        return ['group'=>$grup_user,'socket'=>$socket_url,'subscribe'=>$subscribe];
    }
}
