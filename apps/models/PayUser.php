<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pay_user".
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_package
 * @property float|null $payment
 * @property int|null $payment_status
 * @property int|null $active
 * @property string|null $payment_date
 * @property string|null $data_package
 * @property string|null $expired
 * @property string|null $jatuh_tempo
 */
class PayUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pay_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'id_package'], 'required'],
            [['id_user', 'id_package', 'payment_status', 'active'], 'integer'],
            [['payment'], 'number'],
            [['payment_date', 'data_package', 'expired', 'jatuh_tempo'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_user' => Yii::t('app', 'Id User'),
            'id_package' => Yii::t('app', 'Id Package'),
            'payment' => Yii::t('app', 'Payment'),
            'payment_status' => Yii::t('app', 'Payment Status'),
            'active' => Yii::t('app', 'Active'),
            'payment_date' => Yii::t('app', 'Payment Date'),
            'data_package' => Yii::t('app', 'Data Package'),
            'expired' => Yii::t('app', 'Expired'),
            'jatuh_tempo' => Yii::t('app', 'Jatuh Tempo'),
        ];
    }
}
