<?php

namespace app\models\credential;

use Yii;
use app\models\credential\Users;

/**
 * This is the model class for table "user_setting".
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $api
 * @property int|null $webhook
 * @property int|null $webhook_url
 */
class UserSettings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_setting';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['api', 'webhook'], 'integer'],
            [['api', 'webhook'], 'default','value'=>0],
            [['webhook_url'],'required',
                'when'=>function($model) { return $model->webhook != 0;},
                'whenClient'=>
                <<<JS
                    function (attribute, value) {
                    return $('#country').val() === 'USA';
                }
                JS,

            ],
            [['api', 'webhook', 'webhook_url'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'api' => Yii::t('app', 'RESTFull API'),
            'webhook' => Yii::t('app', 'Using Webhook'),
            'webhook_url' => Yii::t('app', 'Webhook Url'),
        ];
    }

    public function getUser()
    {
        return $this->hasOne(Users::class,['id'=>'user_id']);
    }
}
