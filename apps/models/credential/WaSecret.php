<?php

namespace app\models\credential;

use Yii;
use app\models\credential\Users;

/**
 * This is the model class for table "wa_id".
 *
 * @property int $id
 * @property int $id_user
 * @property string|null $wa_session
 * @property string|null $last_status
 * @property string|null $last_connected
 */
class WaSecret extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'wa_id';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user'], 'required'],
            [['id_user'], 'integer'],
            [['wa_session', 'last_connected','data'], 'safe'],
            [['last_status'], 'string', 'max' => 120],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_user' => Yii::t('app', 'Id User'),
            'wa_session' => Yii::t('app', 'Wa Session'),
            'last_status' => Yii::t('app', 'Last Status'),
            'last_connected' => Yii::t('app', 'Last Connected'),
            'data' => Yii::t('app', 'Data Whatsapp'),
        ];
    }

    public function getUser()
    {
        return $this->hasOne(Users::class,['id'=>'id_user']);
    }
}
