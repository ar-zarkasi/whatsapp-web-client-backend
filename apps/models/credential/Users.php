<?php

namespace app\models\credential;

use Yii;
use yii\web\IdentityInterface;
use yii\db\ActiveRecord;
use app\models\credential\UserSettings;
/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $email
 * @property string|null $phone
 * @property string|null $confirmed_email
 * @property string|null $confirmed_phone
 * @property string $name
 * @property string $auth
 * @property string|null $token
 * @property string $password
 * @property string|null $token_expiry
 * @property int|null $deleted
 * @property string|null $created_date
 * @property string|null $modified_date
 */
class Users extends ActiveRecord implements IdentityInterface
{
    public static $username;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'name', 'auth', 'password'], 'required'],
            [['confirmed_email', 'confirmed_phone', 'token_expiry', 'created_date', 'modified_date'], 'safe'],
            [['password'], 'string'],
            [['deleted'], 'integer'],
            [['email'], 'string', 'max' => 320],
            [['phone'], 'string', 'max' => 14],
            [['name'], 'string', 'max' => 160],
            [['auth'], 'string', 'max' => 100],
            [['token'], 'string', 'max' => 125],
            [['active'],'default','value'=>0],
            [['email', 'phone'], 'unique', 'targetAttribute' => ['email', 'phone']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'confirmed_email' => Yii::t('app', 'Confirmed Email'),
            'confirmed_phone' => Yii::t('app', 'Confirmed Phone'),
            'name' => Yii::t('app', 'Name'),
            'auth' => Yii::t('app', 'Auth'),
            'token' => Yii::t('app', 'Token'),
            'password' => Yii::t('app', 'Password'),
            'token_expiry' => Yii::t('app', 'Token Expiry'),
            'parent_user' => Yii::t('app', "User Induk"),
            'deleted' => Yii::t('app', 'Deleted'),
            'created_date' => Yii::t('app', 'Created Date'),
            'modified_date' => Yii::t('app', 'Modified Date'),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }
    public static function findIdentityByEmail($email)
    {
        return self::find()->where(['email'=>$email])->one();
    }

    public function getUsername()
    {
        return $this->getId();
    }
    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $token = \base64_decode($token);
        $token = \explode('###',$token);
        $m = self::find()->where(['token'=> $token[0]])
        ->andWhere(['auth'=>\base64_decode($token[1])]);
        // ->andWhere(['<=','token_expiry'=>date('Y-m-d H:i:s')]);
        return $m->one();
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        $m = self::find()->where(['email'=> $username])
        ->andWhere(['IS','confirmed_email',new \yii\db\Expression('NOT NULL')]);
        return $m->one();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->auth;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth === $authKey;
    }

    public function getEncodeToken()
    {
        return base64_encode($this->token.'###'.base64_encode($this->auth));
    }

    // Generate For Security (Case for new user)
    public function generateAuthKey(){
        $this->auth = \Yii::$app->security->generateRandomString(16);
    }
    public function generateToken(){
        $this->token = \Yii::$app->security->generateRandomString(20);
    }
    public function generateAccessToken()
    {
        do {
            $this->generateToken(); // generate ulang token key
        } while (static::find()->where(['token' => $this->token])->exists());
        $this->token_expiry = date('Y-m-d H:i:s',strtotime("+1 day"));
        return $this->update(false, ['token','token_expiry']);
    }
    public function generateSecurity($first = true)
    {
        if($first){
            self::generateAuthKey();
            self::generateToken();
        }
        $this->password = self::encryptPassword($this->password);
    }
    // End for Generate

    // Password Purpose
    public function encryptPassword($password){ 
        return md5(base64_encode($this->auth).'##'.md5($password));
    }
    public function validatePassword($pass){
        $pass = $this->encryptPassword($pass);
        return $this->password === $pass;
    }

    // 
    public static function getListAkses() {
        $role = Yii::$app->authManager->getRoles();
        $return = [];
        foreach ($role as $key => $obj) {
            if(Yii::$app->user->identity->akses!='Administrator' && $key=='Administrator')
                continue;
            else
                $return[$key] = $key;
        }
        return $return;
    }
    public function getAkses() {
        $role = Yii::$app->authManager->getRolesByUser($this->id);
        return key($role);
    }

    public function getCredentialWhatsapp()
    {
        return $this->hasOne(\app\models\credential\WaSecret::class,['id_user'=>'id']);
    }
    public function getSubscribe()
    {
        return $this->hasOne(\app\models\PayUser::class,['id_user'=>'id']);
    }
    public function getParentUser()
    {
        return $this->hasOne(self::class,['id'=>'parent_user'])
        ->from(['parent'=>self::tableName()]);
    }

    public function getSetting()
    {
        return $this->hasOne(UserSettings::class,['user_id'=>'id']);
    }
}
