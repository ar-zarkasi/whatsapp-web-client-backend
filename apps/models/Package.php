<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "package".
 *
 * @property int $id
 * @property string|null $fitur
 * @property float|null $price
 * @property string $packagename
 * @property int|null $special
 * @property float|null $price_before
 */
class Package extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'package';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fitur'], 'safe'],
            [['price', 'price_before'], 'number'],
            [['packagename', 'price'], 'required'],
            [['special','price_before'], 'default','value'=>0],
            [['showcase'], 'default','value'=>1],
            [['packagename'], 'string', 'max' => 160],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fitur' => Yii::t('app', 'Fitur'),
            'price' => Yii::t('app', 'Harga'),
            'packagename' => Yii::t('app', 'Nama Paket'),
            'special' => Yii::t('app', 'Special/Recommend Package ?'),
            'price_before' => Yii::t('app', 'Harga Sebelum Diskon'),
            'showcase' => Yii::t('app', 'Tampilkan'),
        ];
    }
}
