<?php

namespace app\models\logs;

use Yii;

/**
 * This is the model class for table "pay_user_history".
 *
 * @property int $id
 * @property int $id_pay_user
 * @property string|null $data_pay
 */
class PayUserHistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pay_user_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_pay_user'], 'required'],
            [['id_pay_user'], 'integer'],
            [['data_pay'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_pay_user' => Yii::t('app', 'Id Pay User'),
            'data_pay' => Yii::t('app', 'Data Pay'),
        ];
    }
}
