<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;
use app\models\credential\Users;

/**
 * ContactForm is the model behind the contact form.
 */
class RegisterForm extends Model
{
    public $name;
    public $email;
    public $phone;
    public $password;
    public $cpassword;
    public $agree = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'phone', 'password','cpassword'], 'required','message'=>'{attribute} Harus Diisi'],
            ['agree','required','message'=>'Anda Belum Menyetujui Terms & Agreement dari kami !'],
            // email has to be a valid email address
            ['email', 'email'],
            [['email','phone'],'validateAccount'],
            [['cpassword'],'compare','compareAttribute'=>'password','skipOnEmpty'=>false, 'message'=>'password tidak sesuai dengan konfirmasi password anda',]
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Nama Lengkap',
            'email' => 'Alamat Email',
            'phone' => 'Nomor Telepon',
            'password' => 'Password',
            'cpassword' => 'Konfirmasi Password',
            'agree' => 'Saya Setuju',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function validateAccount($attribute, $params)
    {
        if($attribute == 'email'){
            $em = Users::find()->where(['email'=>$this->email])->count();
            if($em > 0) $this->addError('email','Email Telah Terdaftar, silahkan reset password atau hubungi customer service kami');
        } else if($attribute == 'phone') {
            $ph = Users::find()->where(['phone'=>$this->phone])->count();
            if($ph > 0) $this->addError('phone','Nomor Telepon Telah Terdaftar, silahkan reset password atau hubungi customer service kami');
        }
    }

    public function proceed()
    {
        $nuser = new Users;
        $nuser->name = $this->name;
        $nuser->email = $this->email;
        $nuser->phone = $this->phone;
        $nuser->password = $this->password;
        $nuser->generateSecurity();
        return $nuser;
    }
}
