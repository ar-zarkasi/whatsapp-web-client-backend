<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;
use app\models\credential\Users as User;
use app\models\Setup;

/**
 * LoginForm is the model behind the login form.
 */
class GroupAksesForm extends Model
{
    public $group_name;
    public $description;
    public $role;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['group_name'], 'required'],
            [['group_name'], 'string','min'=>5,'max'=>30],
            ['description', 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'group_name'=> 'Nama Grup Akses',
            'description' => 'Deskripsi Grup Akses',
        ];
    }

    public function loadRole()
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole($this->group_name);
        if(is_null($role)) $role = $auth->createRole($this->group_name);
        $this->role = $role;
        return $role;
    }

    public static function loadDefaultCustomer()
    {
        $setup = Setup::DefaultSetup()['group'];
        $auth = Yii::$app->authManager;
        $role = $auth->getRole($setup->value);
        return $role;
    }

}
