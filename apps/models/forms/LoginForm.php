<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;
use app\models\credential\Users as User;

/**
 * LoginForm is the model behind the login form.
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = false;
    public $code;
    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // ['rememberMe','default','value'=>false],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
            // activate Captcha when wrong logon 3 times
            ['code','captcha','when'=>function(){return $this->butuhCaptcha;},'caseSensitive'=>false,'captchaAction'=>'site/captcha'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username'=> 'Email',
            'password' => 'Kata Sandi',
            'code' => 'Security Code',
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user) {
                $this->addError('username', 'Incorrect username or not registered.');
                $this->Wrong;
            }
            elseif(empty($user->confirmed_email)) {
                $this->addError('username', 'Email Anda Belum Diaktivasi, Silahkan lakukan aktivasi terlebih dahulu');
                $this->Wrong;
            }
            elseif($user->active != 1) {
                $this->addError('username', 'Akun Anda dinonaktifkan, silahkan hubungi customer service kami');
                $this->Wrong;
            }
            elseif (!$user->validatePassword($this->password)) {
                $this->addError('password', 'Incorrect password.');
                $this->Wrong;
            }
            elseif($user->active != 1){
                $this->addError('username', 'This user not active, please contact Administrator !.');
                $this->Wrong;
            }

            $this->password = '';
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*7 : 0);
        }
        return false;
    }
    
    /* Function Activate Captcha When 3 Times Wrong Logon */
    public function getButuhCaptcha()
    {
        return Yii::$app->session->get('attemp', 0) >= 3;
    }
    public function getWrong(){
        Yii::$app->session->set('attemp', Yii::$app->session->get('attemp', 0)+1);
    }
    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findIdentityByEmail($this->username);
        }

        return $this->_user;
    }
}
