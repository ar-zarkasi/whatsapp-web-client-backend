<?php

namespace app\models\forms;

use Yii;
use yii\helpers\Json;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class HookWa extends Model
{
    public $fromclient;
    public $id;
    public $timestamp;
    public $from;
    public $text;
    public $type;
    public $businessAccount;
    public $enterpriseAccount;
    public $sender;
    public $device;
    public $fromContact;
    public $broadcast;
    public $isForward;

    public function rules()
    {
        return [
            [['fromclient','id','from','type','text'],'required'],
            [['timestamp','businessAccount','enterpriseAccount','device','fromContact','broadcast','isForward'],'safe'],
            [['sender'],'string','strict'=>false],
        ];
    }

    public function getUser()
    {
        $user = \app\models\credential\Users::findIdentityByAccessToken($this->fromclient);
        return $user;
    }
}