<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'wabot_url'=>'http://localhost:8000/',
    'bsVersion'=>'5.x',
];
