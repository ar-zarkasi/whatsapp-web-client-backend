<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'name'=>'Technergy Whatsapp Bot',
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'FiIp3K3Guhc9GjxftgOMcA0y1yLFuKKz',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'session' => [
            'class' => 'yii\web\DbSession',
            // 'db' => 'mydb',
            'sessionTable' => 'session',
        ],
        'user' => [
            'identityClass' => 'app\models\credential\Users',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'assetManager' => [
            'linkAssets' => false,
            'bundles' => [
                'yii\bootstrap\BootstrapAsset' => [
                    'sourcePath' => '@vendor/twbs/bootstrap/dist',
                    'css' => [
                        YII_ENV_DEV ? 'css/bootstrap.css' : 'css/bootstrap.min.css',
                    ],
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'sourcePath' => '@vendor/twbs/bootstrap/dist',
                    'js' => [
                        YII_ENV_DEV ? 'js/bootstrap.js' : 'js/bootstrap.min.js',
                    ]
                ],
            ],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                '/' => 'site/index',
                'login'=> 'site/login',
                'register'=>'register/index',
                'POST logout' => 'site/logout',

                /* '<modules:\w+>/<submodules:\w+>/<controller:\w+>/<action:\[A-Za-z0-9 -_.]+>/<id:\[A-Za-z0-9 -_.]+>'=>'<modules>/<submodules>/<controller>/<action>',
                '<modules:\w+>/<submodules:\w+>/<controller:\w+>/<action:\[A-Za-z0-9 -_.]+>/<id:\d+>'=>'<modules>/<submodules>/<controller>/<action>',
                '<modules:\w+>/<submodules:\w+>/<controller:\w+>/<action:\[A-Za-z0-9 -_.]+>'=>'<modules>/<submodules>/<controller>/<action>', */

                /* '<modules:\w+>/<controller:\w+>/<action:[A-Za-z-_.]+>/<id:[A-Za-z0-9-_.]+>'=>'<modules>/<controller>/<action>',
                '<modules:\w+>/<controller:\w+>/<action:[A-Za-z-_.]+>/<id:\d+>'=>'<modules>/<controller>/<action>',
                '<modules:\w+>/<controller:\w+>/<action:[A-Za-z-_.]+>'=>'<modules>/<controller>/<action>',
                '<modules:\w+>/<action:[A-Za-z-_.]+>/<id:\d+>'=>'<modules>/default/<action>',
                '<modules:\w+>/<controller:\w+>'=>'<modules>/<controller>/index', */
                // '<modules:\w+>/default/<action:\w+>'=>'<modules>/default/<action>',
                
                // '<controller:\w+>' => '<controller>/index',
                // '<controller:\w+>/<action:\w+>/<id:[A-Za-z0-9 -_.]+>'=>'<controller>/<action>',
                [
                    'class' => 'yii\rest\UrlRule', 
                    'controller' => [
                        'whatsapp'=>'whatsapp',
                    ],
                    'extraPatterns' => [],
                    'patterns' => [
                        'GET all' => 'all',
                        'GET <id:[A-Za-z0-9 -_.]+>' => 'index', 
                        'POST disconnected' => 'disconnected',
                        'POST status-update' => 'status-update',
                        'POST delivery-chat' => 'delivery-chat',
                        'POST save-session/<id:[A-Za-z0-9 -_.]+>' => 'save-session',
                    ],
                ],
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        
    ],
    'modules'=>[
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],
        'backoffice' => [
            'class' => '\app\modules\cms\ControlPanel',
            'as access' => [
                // 'class' => 'mdm\admin\components\AccessControl',
                'class' => '\hscstudio\mimin\components\AccessControl',
                'allowActions' => []
            ],
        ],
        'root' => [
            'class' => '\app\modules\root\ModuleAdmin',
            'as access' => [
                // 'class' => 'mdm\admin\components\AccessControl',
                'class' => '\hscstudio\mimin\components\AccessControl',
                'allowActions' => []
            ],
        ],
    ],
    'as access' => [
        // 'class' => 'mdm\admin\components\AccessControl',
        'class' => '\hscstudio\mimin\components\AccessControl',
        'allowActions' => [
            'site/index',
            'site/login',
            'site/register',
            'site/contact-us',
            'site/about',
            'site/terms-agreement',
            'site/privacy',
            'site/error',
            'whatsapp/*',
            'gii/*',
            'debug/*',
            'register/*',
            'register-admin/*',
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
