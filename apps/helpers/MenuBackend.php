<?php
namespace app\helpers;
use hscstudio\mimin\components\Mimin;

class MenuBackend extends \yii\base\BaseObject {
    public static function menu()
    {
        $menu  = [
            [
                'label'=>'Main Menu',
                'options'=>['class'=>'sidebar-header']
            ],
            [
                'label'=>'<i class="align-middle" data-feather="sliders"></i> <span class="align-middle">Dashboard</span>',
                'options'=>['class'=>'sidebar-item'],
                'url'=>['/backoffice'],
            ],
            [
                'label'=>'<i class="align-middle" data-feather="message-circle"></i> <span class="align-middle">Chatbot</span>',
                'options'=>['class'=>'sidebar-item'],
                'url'=>['/backoffice/chatbot'],
            ],
            [
                'label'=>'<i class="align-middle" data-feather="book"></i> <span class="align-middle">Contact</span>',
                'options'=>['class'=>'sidebar-item'],
                'url'=>'#',
            ],
            [
                'label'=>'<i class="align-middle" data-feather="message-square"></i> <span class="align-middle">Messages</span>',
                'options'=>['class'=>'sidebar-item'],
                'url'=>'#',
            ],
            [
                'label'=>'<i class="align-middle" data-feather="users"></i> <span class="align-middle">Agent</span>',
                'options'=>['class'=>'sidebar-item'],
                'url'=>'#',
            ],
            [
                'label'=>'<i class="align-middle" data-feather="cast"></i> <span class="align-middle">Broadcast</span>',
                'options'=>['class'=>'sidebar-item'],
                'url'=>'#',
            ],
            [
                'label'=>'<i class="align-middle" data-feather="settings"></i> <span class="align-middle">Pengaturan</span> <i class="align-end" data-feather="chevron-down"></i>',
                'options'=>['class'=>'sidebar-item'],
                'url'=>'#',
                'submenuTemplate' => "<ul id='settings' class='sidebar-dropdown list-unstyled collapse' data-bs-parent='#sidebar'>{items}</ul>",
                'template'=>'<a href="{url}" data-bs-target="#settings" class="sidebar-link collapsed" data-bs-toggle="collapse">{label}</a>',
                'items'=>[
                    [
                        'label'=>'<i class="fab fa-whatsapp"></i> <span class="align-middle">Whatsapp</span>',
                        'options'=>['class'=>'sidebar-item'],
                        'url'=>['/backoffice/setup'],
                    ],
                    [
                        'label'=>'<i class="align-middle" data-feather="link"></i> <span class="align-middle">API</span>',
                        'options'=>['class'=>'sidebar-item'],
                        'url'=>['/backoffice/setup/webhook'],
                    ],
                ]
            ],
        ];
        return Mimin::filterMenu($menu);
    }

    public static function rootMenu()
    {
        return [
            [
                'label'=>'Root Admin Privilege',
                'options'=>['class'=>'sidebar-header']
            ],
            [
                'label'=>'<i class="align-middle" data-feather="shield"></i> <span class="align-middle">Security</span> <i class="align-end" data-feather="chevron-down"></i>',
                'options'=>['class'=>'sidebar-item'],
                'url'=>'#',
                'submenuTemplate' => "<ul id='security' class='sidebar-dropdown list-unstyled collapse' data-bs-parent='#sidebar'>{items}</ul>",
                'template'=>'<a href="{url}" data-bs-target="#security" class="sidebar-link collapsed" data-bs-toggle="collapse">{label}</a>',
                'items'=>[
                    [
                        'label'=>'<i class="align-middle" data-feather="shield"></i> Route',
                        'options'=>['class'=>'sidebar-item'],
                        'url'=>['/root/mimin/route'],
                    ],
                    /* [
                        'label'=>'<i class="align-middle" data-feather="shield"></i> Permission',
                        'options'=>['class'=>'sidebar-item'],
                        'url'=>['/mimin/permission'],
                    ], */
                    [
                        'label'=>'<i class="align-middle" data-feather="shield"></i> Role',
                        'options'=>['class'=>'sidebar-item'],
                        'url'=>['/root/mimin/role'],
                    ],
                    /* [
                        'label'=>'<i class="align-middle" data-feather="shield"></i> Assignment',
                        'options'=>['class'=>'sidebar-item'],
                        'url'=>['/mimin/user'],
                    ], */
                ],
            ],
            [
                'label'=>'<i class="fas fa-toolbox"></i> <span class="align-middle">Pengaturan Techbot</span> <i class="align-end" data-feather="chevron-down"></i>',
                'options'=>['class'=>'sidebar-item'],
                'url'=>'#',
                'submenuTemplate' => "<ul id='atur' class='sidebar-dropdown list-unstyled collapse' data-bs-parent='#sidebar'>{items}</ul>",
                'template'=>'<a href="{url}" data-bs-target="#atur" class="sidebar-link collapsed" data-bs-toggle="collapse">{label}</a>',
                'items'=>[
                    [
                        'label'=>'<i class="align-middle" data-feather="dollar-sign"></i> Paket Harga',
                        'options'=>['class'=>'sidebar-item'],
                        'url'=>['/root/package'],
                    ],
                    [
                        'label'=>'<i class="align-middle" data-feather="settings"></i> Client State',
                        'options'=>['class'=>'sidebar-item'],
                        'url'=>['/root/default'],
                    ],
                    [
                        'label'=>'<i class="align-middle" data-feather="settings"></i> Setup App',
                        'options'=>['class'=>'sidebar-item'],
                        'url'=>['/root/default/setup'],
                    ],
                ],
            ],
        ];
    }
}