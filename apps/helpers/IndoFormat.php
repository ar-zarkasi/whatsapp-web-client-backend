<?php
namespace app\helpers;

class IndoFormat extends \yii\base\BaseObject {

    public static function listHari()
    {
        return [
            'Minggu',
            'Senin',
            'Selasa',
            'Rabu',
            'Kamis',
            "Jum'at",
            "Sabtu",
        ];
    }
    public static function listHariEn()
    {
        return [
            'Sunday',
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            "Friday",
            "Saturday",
        ];
    }
    public static function convertHari($hariint,$lang = 'id')
    {
        $lang = strtolower($lang);
        if($lang == 'id') return self::listHari()[$hariint];
        elseif($lang == 'en') return self::listHariEn()[$hariint];
    }
}