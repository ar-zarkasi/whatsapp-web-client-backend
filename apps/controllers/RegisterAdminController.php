<?php
namespace app\controllers;
use yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\AccessControl;
use app\models\credential\Users;
use app\models\forms\RegisterForm;
use app\models\forms\GroupAksesForm;

class RegisterAdminController extends Controller {
    public function behaviors() {
        $this->layout = 'login';
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'matchCallback' => function($rule,$action){
                            $user = Users::find()->count();
                            return $user <= 0;
                        },
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $model = new RegisterForm;
        $grup = new GroupAksesForm;
        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post()) && $model->validate() ) {
            $transaction = Yii::$app->db->beginTransaction();
            $auth = Yii::$app->authManager;
            try {
                if($model->agree){
                    $new_user = $model->proceed();
                    $new_user->confirmed_email = $new_user->confirmed_phone = date('Y-m-d H:i:s');
                    $new_user->active = 1;
                    if($new_user->save()) {
                        $user = Users::findOne($new_user->getId());
                        // create role administrator/root
                        $grup->group_name = 'root';
                        $role = $grup->loadRole();
                        $auth->add($role);
                        // Permission wiht Url
                        $permission = $auth->createPermission('Super Access Admin');
                        $permission->description = 'Akses Untuk Ke Semua URL tanpa terkecuali';
                        $auth->add($permission);
                        $url = $auth->createPermission('/*');
                        $auth->add($url);
                        // kondisikan parent Child
                        $auth->addChild($role,$permission);
                        $auth->addChild($permission,$url);
                        // Assign ke User
                        /* var_dump($user);
                        var_dump((string) $user->id);
                        $transaction->rollBack();die(); */
                        $auth->assign($role,(string)$user->id);
                        $transaction->commit(); // commit
                        return $this->redirect(['/site/login']);
                    } else {
                        Yii::$app->session->setFlash('danger',"Gagal Membuat Akun !");
                        $transaction->rollBack();
                    }
                } else {
                    $model->addError('agree','Anda Belum Menyetujui Syarat dan ketentuan');
                    $transaction->rollBack();
                }
            } catch (\Throwable $th) {
                $transaction->rollBack();
                throw $th;
                Yii::$app->session->setFlash('danger',"An Error Occured !");
            }
        }
        return $this->render('index', ['model'=>$model]);
    }
}