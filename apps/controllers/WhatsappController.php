<?php
namespace app\controllers;

use Yii;
use yii\helpers\Json;
use yii\rest\Controller;
use app\models\credential\Users;
use app\models\credential\WaSecret;
use app\models\forms\HookWa;

class WhatsappController extends Controller {
    public function behaviors(){
        Yii::$app->request->parsers = [
                'application/json' => 'yii\web\JsonParser',
                'multipart/form-data' => 'yii\web\MultipartFormDataParser',
        ];
        Yii::$app->request->enableCookieValidation = false;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $behavior = parent::behaviors();
        $sitecors = ['*'];
        $behaviors['corsFilter'] = [
            'class'=>\yii\filters\Cors::class,
            'cors'=>[
                'Origin' => $sitecors,
                'Access-Control-Allow-Method' => ['GET','HEAD','POST','PUT'],
                'Access-Control-Request-Method' => ['GET','HEAD','POST','PUT'],
                'Access-Control-Request-Headers' => $sitecors,
                // 'Access-Control-Allow-Origin' =>  $sitecors,
            ]
        ];
        return $behavior;
    }
    /* public function beforeAction($action)
    {            
        $this->enableCsrfValidation = false;
    } */

    public function actionIndex($id) {
        try {
            $user = $this->findModel($id);
            $wa = $user->credentialWhatsapp;
            if(empty($wa)) $data = null;
            else $data = $wa->wa_session;
            return [
                'status'=>200,
                'data'=>$data,
            ];
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function actionAll()
    {
        $wa = Users::find()->joinWith(['credentialWhatsapp'])->all();
        $data = [];
        foreach ($wa as $key => $was) {
            $whatsapp = $was->credentialWhatsapp;
            array_push($data,[
                'id'=>$was->getEncodeToken(),
                // 'session'=>$whatsapp->wa_session,
                'text'=>'Restoring Session '.$was->name,
            ]);
        }
        return [
            'status'=>200,
            'data'=>$data,
        ];
    }

    private function modelId() {
        $model = new \yii\base\DynamicModel(['id']);
        $model->addRule(['id'],'required',['message'=>'Token Diperlukan']);
        return $model;
    }

    public function actionDisconnected()
    {
        $model = $this->modelId();
        if($model->load(Yii::$app->request->getBodyParams(),'') && $model->validate()){
            try {
                $user = $this->findModel($model->id);
                $wa = $user->credentialWhatsapp;
                if(empty($wa)){
                    return [
                        'status'=>200,
                        'message'=>'Session Selesai, Berhasil Dihapus !',
                    ];
                }
                if($wa->delete()) {
                    return [
                        'status'=>200,
                        'message'=>'Session Selesai, Berhasil Dihapus !',
                    ];
                } else {
                    return [
                        'status'=>422,
                        'data'=>$wa,
                        'message'=>'Gagal Menghapus Session',
                    ];
                }
            } catch (\Throwable $th) {
                throw $th;
            }
        }
    }

    public function actionStatusUpdate()
    {
        $model = new \yii\base\DynamicModel(['id','message','status','name','platform','number','wa_version','device']);
        $model->addRule(['id','status'],'required',['message'=>'{attribute} Diperlukan'])
        ->addRule(['message','name','platform','number','wa_version','device'],'safe');
        if($model->load(Yii::$app->request->getBodyParams(),'') && $model->validate()){
            try {
                $user = $this->findModel($model->id);
                $wa = $user->credentialWhatsapp;
                $wa->last_status = $model->status;
                $wa->last_connected = date('Y-m-d H:i:s');
                $wa->data = Json::encode([
                    'name'=>$model->name,
                    'platform'=>$model->platform,
                    'number'=>$model->number,
                    'wa_version'=>$model->wa_version,
                    'device'=>$model->device,
                ]);
                if($wa->save()) {
                    return [
                        'status'=>200,
                        'message'=>'Session Update Berhasil !',
                    ];
                } else {
                    return [
                        'status'=>422,
                        'data'=>$wa,
                        'message'=>'Gagal Mengupdate Session',
                    ];
                }
            } catch (\Throwable $th) {
                throw $th;
            }
        } 
        return [
            'status'=>422,
            'data'=>$model,
            'message'=>'Error Validation Data',
        ];
    }

    public function actionSaveSession($id) {
        try {
            $user = $this->findModel($id);
            $model = new \yii\base\DynamicModel(['sesi']);
            $model->addRule(['sesi'],'required')->addRule(['sesi'],'string',['min'=>10]);
            if($model->load(Yii::$app->request->getBodyParams(),'') && $model->validate()) {
                $cek = WaSecret::find()->where(['id_user'=>$user->id]);
                if($cek->count() > 0) {
                    $wa = $cek->one();
                    $wa->last_status = 'Refreshed Data';
                } else {
                    $wa = new WaSecret;
                    $wa->id_user = $user->id;
                    $wa->last_status = 'New Sign In';
                }
                $wa->wa_session = $model->sesi;
                $wa->last_connected = date('Y-m-d H:i:s');
                if($wa->save()) {
                    return [
                        'status' => 200,
                        'data' => $wa,
                        'message' => 'Success Registration Whatsapp',
                    ];
                } else {
                    return [
                        'status' => 422,
                        'data'=>$wa,
                        'message'=>'An Error Validation Data',
                    ];
                }
            }
            else {
                return [
                    'status' => 422,
                    'data' => $model,
                    'message' => 'Tidak Dapat Menyimpan Sesi Whatsapp',
                ];
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function actionDeliveryChat() {
        $model = new HookWa;
        $status = 'delivered';
        if($model->load(Yii::$app->request->getBodyParams(),'') && $model->validate()) {
            $user = $model->user;
            $status = 'Delivered and Processed';
        }
        return ['status'=>$status];
    }

    public function findModel($token)
    {
        $user = Users::findIdentityByAccessToken($token);
        if(empty($user)) throw new \yii\web\NotFoundHttpException("User Tidak Ditemukan");
        return $user;
    }
}