<?php
namespace app\controllers;

use Yii;
use yii\filters\VerbFilter;
use app\models\forms\RegisterForm;
use app\models\forms\GroupAksesForm;
use app\models\Package;
use app\models\PayUser;
use app\models\Setup;

class RegisterController extends \yii\web\Controller {
    public function behaviours()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [],
            ],
        ];
    }
    public function actionIndex() {
        $this->layout = 'login';
        $model = new RegisterForm;
        $role = GroupAksesForm::loadDefaultCustomer();
        $payuser = new PayUser;
        $default = Setup::DefaultSetup();
        $package = Package::findOne($default['subscribe']);
        $datenow = new \DateTime();
        $datenow->setTimezone(new \DateTimeZone('Asia/Jakarta'));
        $datenow->modify("+7 days");
        $auth = Yii::$app->authManager;
        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post()) && $model->validate() ) {
            $tr = Yii::$app->db->beginTransaction();
            try {
                if(empty($package)) {
                    Yii::$app->session->setFlash('danger',"Gagal Membuat Akun, silahkan coba kembali !");
                    $tr->rollBack();
                } else {
                    if($model->agree){
                        $new_user = $model->proceed();
                        if($new_user->save()) {
                            // assign role permission access
                            $auth->assign($role, (string) $new_user->id);
                            // subscribe
                            $payuser->id_user = $new_user->id;
                            $payuser->id_package = $package->id;
                            $payuser->active = 1;
                            $payuser->data_package = \yii\helpers\Json::encode($package);
                            $payuser->expired = $datenow->format('Y-m-d H:i:s');
                            $datenow->modify("+3 days");
                            $payuser->jatuh_tempo = $datenow->format('Y-m-d H:i:s');
                            $payuser->save();
                            // send email activation
                            ## code send email
                            // Set Flash Success
                            Yii::$app->session->setFlash('success',"Anda Telah Berhasil Membuat Akun, Silahkan Gunakan untuk Login");
                            $tr->commit();
                            return $this->redirect(['/site/login']);
                        } else {
                            Yii::$app->session->setFlash('danger',"Gagal Membuat Akun, silahkan coba kembali !");
                        }
                    } else {
                        $model->addError('agree','Anda Belum Menyetujui Syarat dan ketentuan');
                        $tr->rollBack();
                    }
                }
                
            } catch (\Throwable $th) {
                //throw $th;
                $tr->rollBack();
            }
        }
        return $this->render('form', ['model'=>$model]);
    }
}