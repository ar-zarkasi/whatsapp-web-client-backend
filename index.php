<?php

require __DIR__ . '/apps/config/init.php';
\app\config\Init::init();

$config = require __DIR__ . '/apps/config/web.php';

(new yii\web\Application($config))->run();
