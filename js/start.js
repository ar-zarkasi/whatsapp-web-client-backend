function isObject(val) {
    if (val === null) { return false; }
    return ((typeof val === 'function') || (typeof val === 'object'));
}
function countdown(element,dateTo) {

    // Get today's date and time
    var now = new Date().getTime();
    const to = new Date(dateTo).getTime();
    // Find the distance between now and the count down date
    const distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    const days = Math.floor(distance / (1000 * 60 * 60 * 24));
    const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    const seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Display the result in the element with id="demo"
    element.innerHTML = days + "Hari " + hours + ":"
        + minutes + ":" + seconds;

    // If the count down is finished, write some text
    if (distance < 0) {
        clearInterval(x);
        element.innerHTML = "EXPIRED";
    }
};
/* var socket = io('http://localhost:8000');
const msg = document.getElementById('message-log');
const imagediv = document.getElementById('image-qr');

socket.on('init', (data) => {
    console.log(data);
    msg.innerHTML = 'Preparing ...';
});
socket.on('qr', (data) => {
    imagediv.setAttribute('src', data.src);
});
socket.on('message', function (data) {
    if (isObject(data.text)) {
        if (data.text.hasOwnProperty('stack')) msg.innerHTML = data.text.stack;
        else if (data.text.hasOwnProperty('message')) msg.innerHTML = data.text.message;
    } else msg.innerHTML = data.text;
});
socket.on('ready', (data) => {
    imagediv.remove();
}) */
